


def filter_elements(pkg, tag):
    return list(filter(lambda k: k[0] == tag, pkg.Elements))


def dn_from_path(path):
    return '/'+'/'.join(path)


def abs_ref(dn, name):
    return f'{dn}/{name}'

def relative_ref(dn, name):
    assert name.startswith(dn)
    return name[len(dn)+1:]
