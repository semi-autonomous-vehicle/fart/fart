# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from jinja2 import Environment, FileSystemLoader

import fart.ar.model as arm
from fart import rostypes as r2
import src.fart.ar.typemap as catalog

def writer(output, fname, mode='w'):
    print("Writing: " + fname)
    path = os.path.dirname(fname)
    os.makedirs(path, exist_ok=True)
    with open(fname, mode) as outfile:
        outfile.write(output)

def py_ref(ref):
    dn, ref_type = catalog.from_type_reference(ref)
    return '.'.join([catalog.namespace_from_dn(dn), ref_type])

def typeof(var, dt):
    return type(var) == dt

class R2Generator():
    """
    Templates based code generator.

    """

    def __init__(self, scope, templates, out_dir):
        self.scope = scope
        self.out_dir = out_dir

        CPATH = os.path.dirname(os.path.realpath(__file__))
        env = Environment(loader = FileSystemLoader(os.path.join(CPATH, templates)),
                          trim_blocks=True,
                          lstrip_blocks=True)
        env.filters['py_ref'] = py_ref
        self.constants_tmpl = env.get_template('constants_tmpl.py')
        self.record_tmpl = env.get_template('record_tmpl.py')

    def render_constants(self, ns, constants, enums):

        primitive = filter(lambda c: c.TypeClass == r2.TypeClass.Primitive, constants)
        arrays = list(filter(lambda c: c.TypeClass == r2.TypeClass.Array, constants))
        records = list(filter(lambda c: c.TypeClass == r2.TypeClass.Record, constants))

        res = self.constants_tmpl.render({'constants': primitive,
                                          'arrays': arrays,
                                          'records': records,
                                          'enums': enums,
                                          'r2': r2
        })
        print('res >>', res)
        writer(res, os.path.join(self.out_dir, ns, 'constants.py'))

    def render_records(self, ns, records):

        for record in records:
            res = self.record_tmpl.render({'record': record,
                                           'typeof': typeof,
                                           'r2': r2
            })
        writer(res, os.path.join(self.out_dir, ns, f'{record.Name}.msg'))

    def render_messages(self, ns, messages):
        pass

    def render_actions(self, ns, actions):
        pass

    def render_parameters(self, ns, parameters):
        pass


if __name__ == '__main__':

    model_dir = '../../test/autosar/ComponentTest'
    model_dir = '../../test/autosar/DataTypeMappingSetTest/'
    model_dir = '../../test/autosar/AllGrammarElements'

    ar_pkg = arm.workspace(model_dir)

    scope = catalog.DatatypeScope(ar_pkg)
    scope.build()
    ar_packages = arm.workspace(model_dir)

    # dt_ctx = catalog.DataTypesContext(ar_packages)
    #
    # constants = catalog.ConstantSpecification(ar_packages, dt_ctx)
    # enums = catalog.EnumSpecification(ar_packages, dt_ctx)
    # records = catalog.RecordSpecification(ar_packages, dt_ctx)

    #print(constants.constant_specification)
    # for const in constants.constant_specification['/Everything']:
    #     print('\nconst >>', const)

    templates = 'templates/py/'
    out_dir = "/tmp/workspace/src"

    r2gen = R2Generator(scope, templates, out_dir)

    # for dn, items in records.record_specification.items():
    #     ns = catalog.namespace_from_dn(dn)
    #     if len(items):
    #         r2gen.render_records(ns, items)
    #print(ar_packages.keys(), constants.constant_specification)
    # print('3enum >>', enums.enum_specification)
    #
    # for pkg in ar_packages.keys():
    #     dn = '/'+'/'.join(pkg)
    #     pkg_consts = constants.constant_specification[dn]
    #     pkg_enums = enums.enum_specification[dn]
    #     print(dn, pkg_enums)
    #     if len(pkg_consts) or len(pkg_enums):
    #         ns = catalog.namespace_from_dn(dn)
    #         r2gen.render_constants(ns, pkg_consts, pkg_enums)
