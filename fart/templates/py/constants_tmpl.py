{% block title%}

# Module constants

{% endblock %}


{%- for c in constants -%}
{{ c.Name }} = {{ c.Value.Value }}  # {{ c.Value.Type }}
{% endfor %}

# Array constants

{% for c in arrays %}
{{ c.Name }} = [  # {{ c.Value.Type }}
    {% for e in c.Value.Elements %}
    {{ e[1].Value }},  # {{ e[1].Type }}
    {% endfor %}
]
{% endfor %}


# Record constants

{% for c in records %}
{{ c.Name }} = {{ c.Value.Type }}(
    {% for tc, e in c.Value.Fields %}
    {% if tc == r2.TypeClass.Primitive%}
    {{ e.Value }},  # {{ e.Type }}
    {% else %}
    {{ e.Ref|py_ref }},  # {{ e.Ref }}
    {% endif %}
    {% endfor -%})
{% endfor %}


# Enums

{% for e in enums %}
{{e.Name}}
{% endfor %}


# FIXME: nested records
