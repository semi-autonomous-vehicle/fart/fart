#
# ENUM {{ enum.name }}
#
{% if  docs %}
{{ format_comment(docs.text) }}
#

{% endif -%}

{%- for en in items -%}
int32 {{en.name }} = {{en.value }} {% if  en.docs %}     # {{ format_comment(en.docs.text) }}
{% else -%}
{% autoescape false %}

{% endautoescape %}
{% endif -%}
{% endfor %}
