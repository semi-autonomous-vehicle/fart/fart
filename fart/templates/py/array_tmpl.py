{% block title%}

# Module array dimentions

{% endblock %}


{%- for a in array -%}
{% if  a.docs %}
{{ format_comment(a.docs.text) }}
{% endif -%}
int32 {{ a.size.size }} = {{ a.dim }}

{% endfor %}
