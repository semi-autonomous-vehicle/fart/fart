#
# RECORD {{ record.name }}
#
{% if  record.Desc  %}
#  {{ record.Desc }}
#

{% endif %}

{%- for el in record.Elements -%}

{% if typeof(el, r2.PrimitiveType)  %}
{{el.Type}} {{el.Name}}
{% endif -%}

{% if typeof(el, r2.RecordRef)  %}
{{ el.Type|py_ref }} {{el.Name}}
{% endif -%}

{% if typeof(el, r2.ArrayType)  %}
{{el.Type|py_ref}} {{el.Name}} {{el.Size}}
{% endif -%}

{% endfor %}
