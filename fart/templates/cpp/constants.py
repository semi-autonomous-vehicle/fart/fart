{% block title%}

# Module constants

{% endblock %}


{%- for c in constants -%}
{{ c.Name }} = {{ c.Value.Value }}  # {{ c.Value.Type }}
{% endfor %}

# Array constants

{% for c in arrays %}
{{ c.Name }} = [  # {{ c.Value.Type }}
    {% for e in c.Value.Elements %}
    {{ e[1].Value }},  # {{ e[1].Type }}
    {% endfor %}
]
{% endfor %}


# Record constants

{% for c in records %}
{{ c.Name }} = {{ c.Value.Type }}(
    {% for e in c.Value.Fields %}
    {{ e[1].Type }} = {{ e[1].Value }},
    {% endfor -%})
{% endfor %}

# FIXME: nested records
