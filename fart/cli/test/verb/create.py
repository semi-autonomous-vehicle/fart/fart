
import os

from argparse import ArgumentError
from rosidl_runtime_py import get_interfaces

from fart.extension import VerbExtension
from fart.cli.ros.api import package_name_completer


class CreateVerb(VerbExtension):
    '''
    Create interface package in SWCD project
    '''

    def add_arguments(self, parser, cli_name):
        parser.add_argument(
            '-o', '--output-dir', nargs='?', default='',
            help="Autosar workspace directory")

        argument = parser.add_argument(
            'interface_name', help='Container node name to unload component from'
        )
        argument.completer = package_name_completer

    def main(self, *, args):
        output_dir = '.'
        if args.output_dir:
            output_dir = args.output_dir

        if not os.path.exists(output_dir):
            raise ArgumentError(f'output directory does not exist: {output_dir}')

        try:
            pkg_name = args.interface_name
            interfaces = get_interfaces([pkg_name])
            pkg_interfaces = interfaces[pkg_name]
            convert_files_to_swcd(pkg_name, pkg_interfaces, output_dir)
        except KeyboardInterrupt:
            pass
        finally:
            pass
