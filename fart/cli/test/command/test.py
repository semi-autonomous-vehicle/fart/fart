
from fart.extension import CommandExtension, add_subparsers_on_demand
from fart.cli.ros.api import get_package_names


class TestCommand(CommandExtension):
    """Test tools."""

    def add_arguments(self, parser, cli_name):
        group = parser.add_mutually_exclusive_group(required=False)
        group.add_argument(
            '--report', '-r', action='store_true',
            help='Print all reports.'
        )

        add_subparsers_on_demand(
            parser, cli_name, '_verb', 'verb', required=False)

    def main(self, *, parser, args):
        """Run translator and print report to terminal based on user input args."""
        if hasattr(args, '_verb'):
            extension = getattr(args, '_verb')
            return extension.main(args=args)

        else:
            print('', list(get_package_names()))
