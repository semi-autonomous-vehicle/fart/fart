
from fart.extension import CommandExtension, add_subparsers_on_demand


class ArCommand(CommandExtension):
    """AUTOSAR tools."""

    def add_arguments(self, parser, cli_name):
        group = parser.add_mutually_exclusive_group(required=False)
        group.add_argument(
            '--report', '-r', action='store_true',
            help='Print all reports.'
        )
         # add arguments and sub-commands of verbs
        add_subparsers_on_demand(
            parser, cli_name, '_verb', 'verb', required=False)

    def main(self, *, parser, args):
        """Run translator and print report to terminal based on user input args."""
        if hasattr(args, '_verb'):
            extension = getattr(args, '_verb')
            return extension.main(args=args)
