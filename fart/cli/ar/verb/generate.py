
from fart.extension import VerbExtension


class GenerateVerb(VerbExtension):
    '''
    Translate ROS interface package into SWCD project
    '''

    def add_arguments(self, parser, cli_name):
        parser.add_argument(
            '-t', '--type', nargs='?', default='',
            help="Name of AUTOSAR type to generate")

    def main(self, *, args):
        try:
            print('>> generate: ', args)
        except KeyboardInterrupt:
            pass
        finally:
            pass
