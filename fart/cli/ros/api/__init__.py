
from ament_index_python import get_packages_with_prefixes


def get_package_names():
    return get_packages_with_prefixes().keys()


def package_name_completer(**kwargs):
    """Callable returning a list of packages names."""
    return get_package_names()
