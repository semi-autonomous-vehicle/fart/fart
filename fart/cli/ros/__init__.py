

entry_points = {
    'api': [],
    'command': [('ros', 'RosCommand')],
    'verb': [('translate', 'TranslateVerb')]
}
