
import os

from argparse import ArgumentError
from rosidl_runtime_py import get_interfaces

from fart.extension import VerbExtension
from fart.cli.ros.api import package_name_completer
from fart.ros.convert import convert_files_to_swcd


class TranslateVerb(VerbExtension):
    '''
    Translate ROS interface package into SWCD project
    '''

    def add_arguments(self, parser, cli_name):
        argument = parser.add_argument(
            'interface_name', help='Container node name to unload component from'
        )
        argument.completer = package_name_completer

    def main(self, *, args):
        output_dir = args.artop_workspace

        if not os.path.exists(output_dir):
            raise ArgumentError(f'output directory does not exist: {output_dir}')

        try:
            pkg_name = args.interface_name
            interfaces = get_interfaces([pkg_name])
            pkg_interfaces = interfaces[pkg_name]
            convert_files_to_swcd(pkg_name, pkg_interfaces, output_dir)
        except KeyboardInterrupt:
            pass
        finally:
            pass
