

entry_points = {
    'api': [],
    'command': [('pkg', 'PkgCommand')],
    'verb': [('create', 'CreateVerb'),
             ('list', 'ListVerb'),
    ]
}
