

entry_points = {
    'api': [],
    'command': [('interface', 'InterfaceCommand')],
    'verb': [('package', 'PackageVerb'),
             ('packages', 'PackagesVerb'),
             ('create', 'CreateVerb'),
    ]
}
