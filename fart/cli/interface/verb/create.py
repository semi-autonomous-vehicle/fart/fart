
import os

from argparse import ArgumentError

from fart.ar.interfacemap import Interfaces
from fart.ar.typemap import DatatypeScope
from fart.extension import VerbExtension
from fart.cli.interface.api import package_name_completer

import fart.ar.model as arm
from fart.ros.idl import convert_ast_to_pkg_interfaces


class CreateVerb(VerbExtension):
    '''
    Create interface package in SWCD project
    '''

    def add_arguments(self, parser, cli_name):

        argument = parser.add_argument(
            'pkg_name', help='Package name which defines interfaces to create'
        )
        argument.completer = package_name_completer

    def main(self, *, args):
        output_dir = os.path.join(args.ros_workspace, args.pkg_name)

        if not os.path.exists(output_dir):
            raise ArgumentError(f'output directory does not exist: {output_dir}')

        try:
            ar_pkg = arm.workspace(args.autosar)
            dt_scope = DatatypeScope(ar_pkg)
            dt_scope.build()

            interfaces = Interfaces(ar_pkg, dt_scope)
            interfaces.build()

            if args.pkg_name.startswith('/'):
                pkg_name = args.pkg_name
            else:
                pkg_name = '/'+args.pkg_name

            _ast = convert_ast_to_pkg_interfaces(dt_scope, interfaces, pkg_name, output_dir)


        except KeyboardInterrupt:
            pass
        finally:
            pass
