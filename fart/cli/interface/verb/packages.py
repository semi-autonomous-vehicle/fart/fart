
import os

from argparse import ArgumentError
from rosidl_runtime_py import get_interfaces

from fart.extension import VerbExtension

import fart.ar.model as arm

class PackagesVerb(VerbExtension):
    '''
    List SWCD packages in project workspace
    '''

    def add_arguments(self, parser, cli_name):
        pass

    def main(self, *, args):

        try:
            ar_pkg = arm.workspace(args.autosar)
            for pkg in ar_pkg.keys():
                print('/'.join(pkg))
        except KeyboardInterrupt:
            pass
        finally:
            pass
