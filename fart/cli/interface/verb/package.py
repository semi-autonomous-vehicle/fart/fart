
import os

from argparse import ArgumentError
from rosidl_runtime_py import get_interfaces

from fart.extension import VerbExtension
from fart.cli.interface.api import package_name_completer

import fart.ar.model as arm
from fart.utils.termcolor import bcolors


class PackageVerb(VerbExtension):
    '''
    Display SWCD package info
    '''

    def add_arguments(self, parser, cli_name):
        argument = parser.add_argument(
            'package_name', help='Container node name to unload component from'
        )
        argument.completer = package_name_completer

    def main(self, *, args):

        try:
            ar_pkg = arm.workspace(args.autosar)
            package_ns = tuple(args.package_name.split('/'))
            if package_ns[0] == '':
                package_ns = package_ns[1:]
            package = ar_pkg[package_ns]

            def select_tags(tags):
                return (el for el in package.Elements if el[0] in tags)

            print('Package - ', args.package_name)

            print('Interfaces:')
            for tag, el in select_tags(['CLIENT-SERVER-INTERFACE', 'SENDER-RECEIVER-INTERFACE']):
                print('\t', el.ShortName)

            print('Mode Switch:')
            for tag, el in select_tags(['MODE-SWITCH-INTERFACE']):
                print('\t', el.ShortName)

            print('Parameters:')
            for tag, el in select_tags(['PARAMETER-INTERFACE']):
                print('\t', el.ShortName)

        except KeyError:
            print(bcolors.FAIL + f'No package \'{args.package_name}\' in {args.autosar} project' + bcolors.ENDC)

        except KeyboardInterrupt:
            pass
        finally:
            pass
