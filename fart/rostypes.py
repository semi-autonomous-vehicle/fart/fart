# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple

from enum import auto, Flag

PrimitiveTypeMap = {
    'bool': lambda value: value.lower() == 'true' and True or False,
    'byte': lambda value: int(value),
    'char': lambda value: int(value),
    'float32': lambda value: float(value),
    'float64': lambda value: float(value),
    'int8': lambda value: int(value),
    'uint8': lambda value: int(value),
    'int16': lambda value: int(value),
    'uint16': lambda value: int(value),
    'int32': lambda value: int(value),
    'uint32': lambda value: int(value),
    'int64': lambda value: int(value),
    'uint64': lambda value: int(value),
    'string': lambda value: value,
}

ENUM_UNDERLINING_TYPE = 'uint32'

class TypeClass(Flag):
    Primitive = auto()
    Array = auto()
    Record = auto()
    ConstantReference = auto()
    Enum = auto()


# Literal/Constant Value specification
Literal = namedtuple('Literal', [
    'Label',
    'Value'
])

Constant = namedtuple('Constant', [
    'Name',
    'TypeClass',
    'Value'
])

PrimitiveValue = namedtuple('Value', [
    'Type',                     # or Field in Record
    'Value'
])

ArrayValue = namedtuple('ArrayValue', [
    'Type',
    'Elements'
])

RecordValue = namedtuple('RecordValue', [
    'Type',
    'Fields'
])

ConstantReference = namedtuple('ConstantReference', [
    'Ref'
])

Enumeration = namedtuple('Enumeration', [
    'Name',
    'IotaValues'
])

EnumValue = namedtuple('EnumValue', [
    'Name',
    'Value'
])

ModeValue = namedtuple('ModeValue', [
    'Name',
    'Initial',
    'Modes'
])

# Type specification
PrimitiveType = namedtuple('PrimitiveType', [
    'Name',
    'BaseType',
    'PrimType',
    'Size',
    'DefaultValue'
])

RecordElement = namedtuple('RecordElement', [
    'Name',
    'TypeRef',
])

RecordType = namedtuple('RecordType', [
    'Name',
    'Type',
    'Desc',
    'RecordElements'
])

# RecordRef = namedtuple('RecordRef', [
#     'Name',
#     'Type',
# ])


ArrayDimention = namedtuple('ArrayDimention', [
    'TypeRef',
    'Size',
    'SizeSemantics'
])

ArrayType = namedtuple('ArrayType', [
    'Name',
    'TypeRef',
    'ArrayDimentions'
])


Namespace = namedtuple('Namespace', [

])


# Interfaces

class IdlType(Flag):
    Service = auto()
    Action = auto()
    Parameters= auto()
    ModeSwitch = auto()


Interface = namedtuple('Interface', [
    'Name',
    'IdlType',
    'Idl',
    'Errors',
])

Message = namedtuple('Message', [
    'MsgName',
    'Annotation',
    'Constatnts',
    'Fields',
])


Service = namedtuple('Service', [
    'SrvName',
    'Request',
    'Response',
])


Action = namedtuple('Action', [
    'ActionName',
    'Goal',
    'Feedback',
    'Result',
    'Errors',
])


Parameter = namedtuple('Parameter', [
    'Name',
    'Category',
    'Type',
    'InitValue',
])

ModeSwitch = namedtuple('ModeSwitch', [
    'ModeName',
    'ModeValue',
])



# ServiceComponentTypeTransformer.java

# PPortPrototype = namedtuple('PPortPrototype', [
#     'ShortName',
# 'SwComponentType',
# 'DataElement',
# 'InitValue',
# 'Operation',
# 'QueueLength',
# ])

# RPortPrototype = namedtuple('RPortPrototype', [
#     'ShortName',
# 'DataElement',
# 'HandleTimeoutType',
# 'InitValue',
# 'Operation',
# 'QueueLength',
# ])

# PrPortPrototype = namedtuple('PrPortPrototype', [
#     'ShortName',
# 'SwComponentType',
# 'DataElement',
# 'InitValue',
# 'Operation',
# 'QueueLength',
# 'DataElement',
# 'HandleTimeoutType',
# 'InitValue',
# 'Operation',
# 'QueueLength',

# ])
