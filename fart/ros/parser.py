
# from rosidl_runtime_py.utilities import *

# import rosidl_parser

# topic_type_str = 'map_msgs/msg/OccupancyGridUpdate'

# message_class = get_message(topic_type_str)
# if message_class is None:
#     message = 'message class "{}" is invalid'.format(topic_type_str)
#     return [], message

# message_type = get_message_namespaced_type(topic_type_str)


# for name in message_class.__slots__:
#     slot_index = message_class.__slots__.index(name)
#     current_type = message_class.SLOT_TYPES[slot_index]
#     print(name, current_type)


# current_type.typename # 'int32'


import os
import sys

from rosidl_runtime_py import get_interface_path
from rosidl_adapter.parser import parse_message_file, parse_service_file, parse_action_file


def parse(interface_identifier: str):
    '''
    Parse ROS msg file format
    '''
    parts = interface_identifier.split('/')

    if len(parts) != 3:
        raise ValueError(
            f"Invalid name '{interface_identifier}'. Expected three parts separated by '/'"
        )

    pkg_name, ext, msg_name = parts
    filename = get_interface_path(interface_identifier)

    try:
        if ext == 'msg':
            pkg_msg = parse_message_file(pkg_name, filename)
        elif ext == 'srv':
            pkg_msg = parse_service_file(pkg_name, filename)
        elif ext == 'action':
            pkg_msg = parse_action_file(pkg_name, filename)
        else:
            raise ValueError(f'{ext} not a interface type')
    except Exception as e:
        print(' ', pkg_name, filename, str(e))
        raise

    return pkg_name, ext, pkg_msg


if __name__ == '__main__':

    from rosidl_runtime_py import get_interfaces

    pkg_name = 'tf2_msgs'
    try:
        interfaces = get_interfaces([pkg_name])
    except LookupError as e:
        print(' ', pkg_name, str(e))
        raise

    for interface_name in interfaces[pkg_name]:
        topic = f'{pkg_name}/{interface_name}'
        print(topic)

        try:
            _, _, spec = parse(topic)
            print(spec)
        except Exception as e:
            print(' ', topic, str(e))
            raise
