
import pathlib

from fart.resource import expand_template
from fart.utils.termcolor import bcolors
from fart.ros.parser import parse


def get_include_file(base_type):
    if base_type.is_primitive_type():
        return None
    return f'{base_type.pkg_name}.msg.{base_type.type}'


def get_swcd_type(type_):
    if isinstance(type_, str):
        identifier = type_
    elif type_.is_primitive_type():
        identifier = type_.type
        if (
            identifier in ('string', 'wstring') and
            type_.string_upper_bound is not None
        ):
            identifier += f'<{type_.string_upper_bound}>'
    else:
        identifier = f'{type_.pkg_name}.msg.{type_.type}' # FIXME: pkg/name
        identifier = f'{type_.type}'

    if isinstance(type_, str) or not type_.is_array:
        return identifier

    if type_.is_fixed_size_array():
        return f'{identifier}[{type_.array_size}]'

    if not type_.is_upper_bound:
        return f'sequence<{identifier}>'

    return f'sequence<{identifier}, {type_.array_size}>'


def to_swcd_literal(idl_type, value):
    if idl_type[-1] == ']' or idl_type.startswith('sequence<'):
        elements = [repr(v) for v in value]
        while len(elements) < 2:
            elements.append('')
        return '"(%s)"' % ', '.join(e.replace('"', r'\"') for e in elements)

    if 'boolean' == idl_type:
        return 'true' if value else 'false'
    if idl_type.startswith('string'):
        return string_to_swcd_string_literal(value)
    if idl_type.startswith('wstring'):
        return string_to_swcd_string_literal(value)
    return value


def string_to_swcd_string_literal(string):
    estr = string.encode().decode('unicode_escape')
    estr = estr.replace('"', r'\"')
    return '"{0}"'.format(estr)


def convert_msg_to_swcd(package_name, spec, output_dir):
    output_file = output_dir / pathlib.Path(spec.msg_name).with_suffix('.swcd').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': package_name,
        'relative_input_file': 'msg',
        'msg': spec,
    }

    expand_template('msg.swcd.em', data, output_file, encoding='iso-8859-1')
    return output_file


def convert_srv_to_swcd(package_name, spec, output_dir):
    output_file = output_dir / pathlib.Path(spec.srv_name).with_suffix('.swcd').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': package_name,
        'relative_input_file': 'srv',
        'srv': spec,
    }

    expand_template('srv.swcd.em', data, output_file, encoding='iso-8859-1')
    return output_file


def convert_act_to_swcd(package_name, spec, output_dir):
    output_file = output_dir / pathlib.Path(spec.action_name).with_suffix('.swcd').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': package_name,
        'relative_input_file': 'action',
        'action': spec,
    }

    expand_template('action.swcd.em', data, output_file, encoding='iso-8859-1')
    return output_file


conversion_function_map = {
    'msg': convert_msg_to_swcd,
    'srv': convert_srv_to_swcd,
    'action': convert_act_to_swcd,
}


def convert_files_to_swcd(package_name, interfaces, output_dir):
    '''
    Translate ROS interface package to SWCD project with interface definition
    '''
    package_dir = pathlib.Path(output_dir) / package_name
    package_dir.mkdir(parents=True, exist_ok=True)

    for interface_name in interfaces:
        try:
            topic = f'{package_name}/{interface_name}'
            pkg_name, ext, spec = parse(topic)
        except Exception as e:
            print(bcolors.FAIL + ' ' + topic + str(e) + bcolors.ENDC)
            raise

        conversion_function = conversion_function_map[ext]
        conversion_function(package_name, spec, package_dir / ext)

    dot_project = f'''<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
<name>{package_name}</name>
<comment></comment>
<projects>
</projects>
<buildSpec>
<buildCommand>
<name>org.eclipse.xtext.ui.shared.xtextBuilder</name>
<arguments>
</arguments>
</buildCommand>
</buildSpec>
<natures>
<nature>org.artop.aal.workspace.autosarnature</nature>
<nature>org.eclipse.xtext.ui.shared.xtextNature</nature>
</natures>
</projectDescription>

'''

    project_file = package_dir / '.project'
    project_file.write_text(dot_project)


if __name__ == '__main__':

    from rosidl_runtime_py import get_interfaces


    output_dir = '/tmp'

    pkg_name = 'map_msgs'
    interfaces = get_interfaces([pkg_name])
    pkg_interfaces = interfaces[pkg_name]
    pkg_interfaces = ['msg/PointCloud2Update', 'srv/GetMapROI']

    pkg_name = 'tf2_msgs'
    interfaces = get_interfaces([pkg_name])
    #pkg_interfaces = ['action/LookupTransform']
    pkg_interfaces = interfaces[pkg_name]

    convert_files_to_swcd(pkg_name, pkg_interfaces, output_dir)
