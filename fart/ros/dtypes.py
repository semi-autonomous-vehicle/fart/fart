import functools

import fart.rostypes as r2


def to_ros_literal(type_ref, value):
    return value


def get_ros_typedef(pkg_name, scope, name, type_ref):
    type_def = None
    type_suffix = None
    name_conjugate = name

    type_class, dtype = scope.dereference(type_ref)

    if type_class == r2.TypeClass.ConstantReference:
        type_class, dtype = scope.dereference(dtype.ConstantRef)

    if type_class == r2.TypeClass.Primitive:
        type_def = dtype.PrimType
        if dtype.DefaultValue:
            name_conjugate += f' = {to_ros_literal(dtype, dtype.DefaultValue)}'

    elif type_class == r2.TypeClass.Array:
        array_type_class, array_dtype = scope.dereference(dtype.TypeRef)
        if array_type_class == r2.TypeClass.Primitive:
            type_def = array_dtype.PrimType
        if array_type_class == r2.TypeClass.Enum:
            type_def = r2.ENUM_UNDERLINING_TYPE
        if array_type_class == r2.TypeClass.Record:
            type_def = array_dtype.Type

        dim_str = functools.reduce(
            lambda acc, br: acc +
            (br.SizeSemantics == 'FIXED-SIZE' and f'[{br.Size}]' or '[]'),
            dtype.ArrayDimentions, ''
        )
        type_def += dim_str

    elif type_class == r2.TypeClass.Record:
        type_def = dtype.Type
    elif type_class == r2.TypeClass.Enum:
        type_def = r2.ENUM_UNDERLINING_TYPE

    if type_def.startswith(pkg_name):
        type_def = type_def[len(pkg_name)+1:]

    if type_suffix:
        name_conjugate += type_suffix

    return type_def, name_conjugate
