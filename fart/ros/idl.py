
import pathlib

from rosidl_adapter.parser import ServiceSpecification, MessageSpecification, ActionSpecification

from fart.utils.termcolor import bcolors

import fart.ar.model as arm
import fart.rostypes as r2

from fart.ar.typemap import DatatypeScope
from fart.ar.interfacemap import Interfaces
from fart.resource import expand_template


def _convert_ast_to_msg(pkg_name, dt_cope, spec, package_dir):
    output_file = package_dir / 'msgs' / pathlib.Path(spec.MsgName).with_suffix('.msg').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': pkg_name,
        'scope': dt_cope,
        'msg': spec,
    }
    expand_template('msg.ros.em', data, output_file, encoding='iso-8859-1')
    return output_file


def _convert_ast_to_srv(pkg_name, dt_cope, spec, package_dir):
    output_file = package_dir / 'srv' / pathlib.Path(spec.SrvName).with_suffix('.srv').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': pkg_name,
        'scope': dt_cope,
        'srv': spec,
    }
    expand_template('srv.ros.em', data, output_file, encoding='iso-8859-1')
    return output_file


def _convert_ast_to_action(pkg_name, dt_cope, spec, package_dir):
    output_file = package_dir / 'action' / pathlib.Path(spec.ActionName).with_suffix('.act').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': pkg_name,
        'scope': dt_cope,
        'action': spec,
    }
    expand_template('action.ros.em', data, output_file, encoding='iso-8859-1')
    return output_file


def _convert_ast_to_modeswitch(pkg_name, dt_cope, spec, package_dir):
    output_file = package_dir / 'msgs' / pathlib.Path(spec.ModeName).with_suffix('.msg').name
    print(bcolors.OKGREEN + f'Writing output file: {output_file}' + bcolors.ENDC)

    data = {
        'pkg_name': pkg_name,
        'scope': dt_cope,
        'mode_switch': spec,
    }
    expand_template('modeswitch.ros.em', data, output_file, encoding='iso-8859-1')
    return output_file


def convert_ast_to_pkg_interfaces(dt_scope, interfaces, pkg_name, output_dir):
    package_dir = pathlib.Path(output_dir) / pkg_name[1:]
    pkg_interfaces = interfaces(pkg_name)

    for itf in pkg_interfaces:
        if itf.IdlType.value == r2.IdlType.Service.value:
            _convert_ast_to_srv(pkg_name, dt_scope, itf.Idl, package_dir)
        elif itf.IdlType.value == r2.IdlType.Action.value:
            for op in itf.Idl:
                _convert_ast_to_action(pkg_name, dt_scope, op, package_dir)
        elif itf.IdlType.value == r2.IdlType.ModeSwitch.value:
            for mode in itf.Idl:
                _convert_ast_to_modeswitch(pkg_name, dt_scope, mode, package_dir)

    # msgs
    for record in dt_scope.references.records(pkg_name):
        msg = r2.Message(record.Name,
                         record.Desc,
                         'constatnts',
                         record.RecordElements)

        _convert_ast_to_msg(pkg_name, dt_scope, msg, package_dir)

    # constants


if __name__ == '__main__':
    import os
    model_dir = os.path.dirname(__file__) + '/../../test/autosar/AllGrammarElements'
    ar_pkg = arm.workspace(model_dir)

    dt_scope = DatatypeScope(ar_pkg)
    dt_scope.build()

    interfaces = Interfaces(ar_pkg, dt_scope)
    interfaces.build()

    output_dir = '/tmp'
    package_name = '/Everything'
    output_dir = '/tmp'

    ast = convert_ast_to_pkg_interfaces(dt_scope, interfaces, package_name, output_dir)
