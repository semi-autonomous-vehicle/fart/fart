"""
Convert AR representation to Interface IDL specification.

Provission Sender-Receive, Client-Server, ModeSwitch and Parameters
"""

from collections import namedtuple

from rosidl_adapter.parser import SERVICE_RESPONSE_MESSAGE_SUFFIX, SERVICE_REQUEST_MESSAGE_SUFFIX, ACTION_GOAL_SUFFIX, \
    ACTION_FEEDBACK_SUFFIX, ACTION_RESULT_SUFFIX


from fart.ar.typemap import DatatypeScope
import fart.rostypes as r2

from fart.utils.termcolor import bcolors
from fart.utils import dn_from_path, filter_elements, relative_ref


class Interfaces:
    """Interface declarations."""

    Idl = namedtuple('Idl', [
        'msgs',
        'srv',
        'action',
    ])

    def __init__(self, ar_packages, dt_scope):
        """Interfaces ctor."""
        self.ar_packages = ar_packages
        self.dt_scope = dt_scope

        self.interface_specification = {}

    def __call__(self, pkg_name):
        return self.interface_specification.get(pkg_name, None)

    def build(self):
        """Build Interface specification."""
        for path, pkg in self.ar_packages.items():
            dn = dn_from_path(path)
            self.interface_specification[dn] = self._resolve_interface_type(dn, pkg)

    def _resolve_interface_type(self, dn, pkg):
        interfaces = []

        sender_receiver_interface_spec = filter_elements(pkg, 'SENDER-RECEIVER-INTERFACE')
        for _, spec in sender_receiver_interface_spec:
            interfaces.append(self._resolve_sender_receiver_interface(dn, spec))

        client_server_interface_spec = filter_elements(pkg, 'CLIENT-SERVER-INTERFACE')
        for _, spec in client_server_interface_spec:
            interfaces.append(self._resolve_client_server_interface(dn, spec))

        mode_switch_interface_spec = filter_elements(pkg, 'MODE-SWITCH-INTERFACE')
        for _, spec in mode_switch_interface_spec:
            interfaces.append(self._resolve_mode_switch_interface(dn, spec))

        parameter_interface_interface_spec = filter_elements(pkg, 'PARAMETER-INTERFACE')
        for _, spec in parameter_interface_interface_spec:
            interfaces.append(self._resolve_parameter_interface_interface(dn, spec))

        return interfaces

    def _resolve_sender_receiver_interface(self, dn, spec):
        name = spec.ShortName
        srv_name = name # same as interface
        errors = None
        response = r2.Message(srv_name + SERVICE_RESPONSE_MESSAGE_SUFFIX, '', [], [])

        annotation = None # FIXME: Desc
        constatnts = None # TODO: decide on constatnts

        request = r2.Message(srv_name + SERVICE_REQUEST_MESSAGE_SUFFIX,
                             annotation,
                             constatnts,
                             [r2.RecordElement(el.ShortName, el.TypeTref)
                              for el in spec.DataElements])

        return r2.Interface(name,
                            r2.IdlType.Service,
                            r2.Service(srv_name, request, response),
                            errors)

    def _resolve_client_server_interface(self, dn, spec):
        name = spec.ShortName
        errors = r2.Enumeration(f'{name}', [r2.EnumValue(e[0], e[1])
                                            for e in spec.PossibleErrors])

        operations = []
        for op in spec.Operations:
            action_name = op.ShortName
            op_errors = op.PossibleErrors

            goal = r2.Message(
                action_name + ACTION_GOAL_SUFFIX,
                'annotation',
                'constatnts',
                [r2.RecordElement(el.ShortName, el.TypeTref)
                 for el in filter(lambda ar: ar.Direction == 'IN', op.Arguments)])

            feedback = r2.Message(
                action_name + ACTION_FEEDBACK_SUFFIX,
                'annotation',
                'constatnts',
                [r2.RecordElement(el.ShortName, el.TypeTref)
                 for el in filter(lambda ar: ar.Direction == 'INOUT', op.Arguments)])

            result = r2.Message(
                action_name + ACTION_RESULT_SUFFIX,
                'annotation',
                'constatnts',
                [r2.RecordElement(el.ShortName, el.TypeTref)
                 for el in filter(lambda ar: ar.Direction == 'OUT', op.Arguments)])

            operations.append(r2.Action(action_name, goal, feedback, result, op_errors))

        return r2.Interface(name,
                            r2.IdlType.Action,
                            operations,
                            errors)

    def _resolve_mode_switch_interface(self, dn, spec):
        name = spec.ShortName
        mode_groups = self.dt_scope.modes(dn)

        modes = []
        for mode in spec.ModeGroup:
            mode_name = mode.ShortName
            mode_dt = mode.TypeTref
            mode_value = next((m for m in mode_groups
                               if m.Name == relative_ref(dn, mode_dt)), None)
            modes.append(r2.ModeSwitch(mode_name, mode_value))

        return r2.Interface(name,
                            r2.IdlType.ModeSwitch,
                            modes,
                            [])

    def _resolve_parameter_interface_interface(self, dn, spec):
        name = spec.ShortName

        return r2.Interface(name,
                            r2.IdlType.Parameters,
                            [r2.Parameter(p.ShortName, p.Category, p.TypeTref, p.InitValue)
                             for p in spec.Parameters],
                            [])


if __name__ == '__main__':
    import os
    import fart.ar.model as arm

    model_dir = os.path.dirname(__file__) + '/../../test/autosar/AllGrammarElements'

    ar_pkg = arm.workspace(model_dir)

    dt_scope = DatatypeScope(ar_pkg)
    dt_scope.build()

    interfaces = Interfaces(ar_pkg, dt_scope)
    interfaces.build()
    print(interfaces)
