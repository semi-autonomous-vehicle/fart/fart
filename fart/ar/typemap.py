# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple

from fart import artypes as ar, rostypes as r2

from fart.utils.termcolor import bcolors
from fart.utils import dn_from_path, filter_elements, abs_ref


class DataTypesContext:
    """
    A data type mapping set is used to map application datatypes to
    implementation datatypes.

    """

    ApplicationDataType = namedtuple('ApplicationDataType', [
        'ApplicationPrimitiveDataType',
        'ApplicationArrayDataType',
        'ApplicationRecordDataType',
        'CompuMethod',              # enum definition
        'ModeDeclaration',
    ])

    def __init__(self, ar_packages):
        self.sw_base_types = {}             # SW BASE TYPES
        self.application_data_type = None
        self.data_type_mapping = {}  # map app type to (DEST, impl type)
        self.implementation_data_types = {}

        application_primitive = {}
        application_array = {}
        application_record = {}
        compu_method = {}
        mode_declaration = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)

            for _, base_type in filter_elements(pkg, 'SW-BASE-TYPE'):
                self.sw_base_types[f'{dn}/{base_type.ShortName}'] = base_type.NativeDeclaration

            application_primitive.update(
                {f'{dn}/{impl_dt.ShortName}': impl_dt for _, impl_dt in
                 filter_elements(pkg, 'APPLICATION-PRIMITIVE-DATA-TYPE')})
            application_array.update(
                {f'{dn}/{impl_dt.ShortName}': impl_dt for _, impl_dt in
                 filter_elements(pkg, 'APPLICATION-ARRAY-DATA-TYPE')})
            application_record.update(
                {f'{dn}/{impl_dt.ShortName}': impl_dt for _, impl_dt in
                 filter_elements(pkg, 'APPLICATION-RECORD-DATA-TYPE')})
            compu_method.update(
                {f'{dn}/{impl_dt.ShortName}': impl_dt for _, impl_dt in
                 filter_elements(pkg, 'COMPU-METHOD')})
            mode_declaration.update(
                {f'{dn}/{impl_dt.ShortName}': impl_dt for _, impl_dt in
                 filter_elements(pkg, 'MODE-DECLARATION-GROUP')})

            for _, impl_dt in filter_elements(pkg, 'IMPLEMENTATION-DATA-TYPE'):
                self.implementation_data_types[abs_ref(dn, impl_dt.ShortName)] = impl_dt

            for _, dt_map_set in filter_elements(pkg, 'DATA-TYPE-MAPPING-SET'):
                for dt_map in dt_map_set.DataTypeMaps:
                    app_dest, app_ref = dt_map.ApplicationDataTypeRef
                    self.data_type_mapping[app_ref] = app_dest, dt_map.ImplementationDataTypeRef

        self.application_data_type = DataTypesContext.ApplicationDataType(
            application_primitive,
            application_array,
            application_record,
            compu_method,
            mode_declaration,
        )

    def get_application_primitive(self, app_ref):
        return self.application_data_type.ApplicationPrimitiveDataType[app_ref]

    def get_application_array(self, app_ref):
        return self.application_data_type.ApplicationArrayDataType[app_ref]

    def get_application_record(self, app_ref):
        return self.application_data_type.ApplicationRecordDataType[app_ref]

    def get_compu_method(self, app_ref):
        return self.application_data_type.CompuMethod[app_ref]

    def get_mode_declaration(self, app_ref):
        return self.application_data_type.ModeDeclaration[app_ref]

    def impl_ref_from_app_ref(self, app_ref):
        dest, impl_ref = self.data_type_mapping[app_ref]
        return dest, impl_ref

    def get_implementation(self, impl_ref):
        return self.implementation_data_types[impl_ref]

    def get_sw_base_type(self, base_type):
        base_type = self.sw_base_types.get(base_type, base_type)
        return base_type


class TypeSpecification:
    """
    Base for primitive and composite data types
    """
    def __init__(self, dt_ctx):
        self.dt_ctx = dt_ctx
        self.references = {}

    def get_impl_ref(self, impl_ref):
        return self.references.get(impl_ref, None)

    def get_app_ref(self, app_ref):
        _, impl_ref = self.dt_ctx.impl_ref_from_app_ref(app_ref)
        return self.get_impl_ref(impl_ref)

class PrimitiveSpecification(TypeSpecification):
    """
    All primitive types may be associated with a software base type
    (Primitive types include: Boolean, Integer, Real, and String).

    """
    def __init__(self, ar_packages, dt_ctx):
        super().__init__(dt_ctx)

        self.primitive_specification = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)

            primitive_spec = filter_elements(pkg, 'APPLICATION-PRIMITIVE-DATA-TYPE')

            self.primitive_specification[dn] = self.resolve_primitive_type(dn, primitive_spec)

    def __call__(self, pkg_name):
        return self.primitive_specification.get(pkg_name, None)

    def resolve_primitive_type(self, dn, primitive_spec):
        primitives = []
        for _, spec in primitive_spec:

            # skip enums
            if spec.CompuMethod:
                continue

            default = None
            size = None
            short_name = spec.ShortName
            app_ref = abs_ref(dn, short_name)
            # app_dt = self.dt_ctx.get_application_primitive(app_ref)
            _, impl_ref = self.dt_ctx.impl_ref_from_app_ref(app_ref)
            impl_dt = self.dt_ctx.get_implementation(impl_ref)

            if spec.Category == 'VALUE':
                base_type = impl_dt.BaseType
                prim_type = self.dt_ctx.get_sw_base_type(impl_dt.BaseType)

                if not prim_type:
                    print(bcolors.WARNING + f'Warning: {app_ref} does not extends to native_declaration' + bcolors.ENDC)

            elif spec.Category == 'BOOLEAN':
                base_type = impl_dt.BaseType
                prim_type = 'bool'

            elif spec.Category == 'STRING':
                base_type = impl_dt.BaseType
                prim_type = 'string'
                if len(impl_dt.SubElements):
                    size = int(impl_dt.SubElements[0].ArraySize)
            else:
                raise(RuntimeError(f'{app_ref} in unhandled category: {impl_dt.Category}'))

            primitive = r2.PrimitiveType(short_name, base_type, prim_type, size, default)
            primitives.append(primitive)

            self.references[impl_ref] = primitive

        return primitives


class ArraySpecification(TypeSpecification):
    """
    An array declaration
    """
    def __init__(self, ar_packages, dt_ctx):
        super().__init__(dt_ctx)

        self.array_specification = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)

            array_spec = filter_elements(pkg, 'APPLICATION-ARRAY-DATA-TYPE')

            self.array_specification[dn] = self.resolve_array_type(dn, array_spec)

    def __call__(self, pkg_name):
        return self.array_specification.get(pkg_name, None)

    def resolve_array_type(self, dn, array_spec):
        arrays = []
        for _, spec in array_spec:
            short_name = spec.ShortName
            app_ref = abs_ref(dn, short_name)
            app_dt = self.dt_ctx.get_application_array(app_ref)
            _, impl_ref = self.dt_ctx.impl_ref_from_app_ref(app_ref)
            impl_dt = self.dt_ctx.get_implementation(impl_ref)

            app_type_ref = app_dt.Element.Type

            dimensions = []
            for el_impl_dt in impl_dt.SubElements:
                dimensions.append(r2.ArrayDimention(el_impl_dt.ImplementationDataTypeRef,
                                                    el_impl_dt.ArraySize,
                                                    el_impl_dt.ArraySizeSemantics))

            arr = r2.ArrayType(short_name, app_type_ref, dimensions)
            arrays.append(arr)

            self.references[impl_ref] = arr

        return arrays


class RecordSpecification(TypeSpecification):
    """
    A record declaration
    """
    def __init__(self, ar_packages, dt_ctx):
        super().__init__(dt_ctx)

        self.record_specification = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)

            record_spec = filter_elements(pkg, 'APPLICATION-RECORD-DATA-TYPE')

            self.record_specification[dn] = self.resolve_record_type(dn, record_spec)

    def __call__(self, pkg_name):
        return self.record_specification.get(pkg_name, None)

    def resolve_record_type(self, dn, record_spec):
        records = []
        for _, spec in record_spec:
            short_name = spec.ShortName
            app_ref = abs_ref(dn, short_name)
            app_dt = self.dt_ctx.get_application_record(app_ref)
            _, impl_ref = self.dt_ctx.impl_ref_from_app_ref(app_ref)
            impl_dt = self.dt_ctx.get_implementation(impl_ref)

            elements = []
            for el_app_dt, el_impl_dt in zip(app_dt.Elements, impl_dt.SubElements):
                elements.append(r2.RecordElement(el_app_dt.ShortName, el_app_dt.Type))

            record = r2.RecordType(short_name, app_ref, app_dt.Desc, elements)
            records.append(record)

            self.references[impl_ref] = record

        return records


class EnumSpecification(TypeSpecification):
    """
    Enumerations in AUTOSAR are not primitive datatypes. Rather a range of
    integers can be used as a structural description.
    """
    def __init__(self, ar_packages, dt_ctx):
        super().__init__(dt_ctx)

        self.enum_specification = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)
            pkg = ar_packages[path]

            enum_spec = filter_elements(pkg, 'COMPU-METHOD')

            application_ptimitive_spec = filter(lambda el: el[1].CompuMethod,
                                                filter_elements(pkg, 'APPLICATION-PRIMITIVE-DATA-TYPE'))

            self.enum_specification[dn] = self.resolve_enum_type(dn, enum_spec, application_ptimitive_spec)

    def __call__(self, pkg_name):
        return self.enum_specification.get(pkg_name, None)

    def resolve_enum_type(self, dn, enum_spec, application_ptimitive_spec):

        values_map = {}
        for _, spec in enum_spec:
            app_ref = abs_ref(dn, spec.ShortName)
            elements = [(scale.VT, scale.LowerLimit) for scale in spec.CompuScales]
            values_map[app_ref] = elements

        enums = []
        for _, app_spec in application_ptimitive_spec:
            short_name = app_spec.ShortName
            elements = values_map[app_spec.CompuMethod]
            enum = r2.Enumeration(short_name, elements)
            enums.append(enum)

            app_ref = abs_ref(dn, short_name)
            _, impl_ref = self.dt_ctx.impl_ref_from_app_ref(app_ref)
            self.references[impl_ref] = enum

        return enums


class ValueSpecification:
    """
    Value specification base
    """
    def __init__(self, dt_ctx):
        self.dt_ctx = dt_ctx


class ModeSpecification(ValueSpecification):
    """
    Mode in AUTOSAR are not primitive datatypes. Rather a range of
    integers can be used as a structural description.
    """
    def __init__(self, ar_packages, dt_ctx):
        super().__init__(dt_ctx)

        self.mode_specification = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)

            mode_spec = filter_elements(pkg, 'MODE-DECLARATION-GROUP')

            self.mode_specification[dn] = self.resolve_mode_type(dn, mode_spec)

    def __call__(self, pkg_name):
        return self.mode_specification.get(pkg_name, None)

    def resolve_mode_type(self, _dn, mode_spec):
        modes = []
        for _, spec in mode_spec:
            short_name = spec.ShortName

            modes_decl = spec.ModeDeclarations
            initial_mode = spec.InitialMode
            mode = r2.ModeValue(short_name, initial_mode, modes_decl)
            modes.append(mode)

        return modes


class ConstantSpecification(ValueSpecification):
    """
    A constant declaration
    """
    def __init__(self, ar_packages, dt_ctx, references):
        super().__init__(dt_ctx)

        self.references = references
        self.constant_specification = {}

        for path, pkg in ar_packages.items():
            dn = dn_from_path(path)

            const_spec = filter_elements(pkg, 'CONSTANT-SPECIFICATION')

            self.constant_specification[dn] = self.resolve_value_type(dn, const_spec)

    def __call__(self, pkg_name):
        return self.constant_specification.get(pkg_name, None)

    def resolve_value_type(self, dn, const_spec):
        constants = []
        for _, spec in const_spec:
            name = spec.ShortName

            type_class, constant_value = self.resolve_variation_point(dn, spec.ValueSpecification)
            constant = r2.Constant(name, type_class, constant_value)
            constants.append(constant)

        return constants

    def resolve_variation_point(self, dn, spec, base_type=None):
        variation_point = spec.VariationPointType
        if variation_point == ar.VariationPointEnum.ConstantReference:
            type_class = r2.TypeClass.ConstantReference
            type_value = r2.ConstantReference(spec.VariationPoint.ConstantRef)
            return type_class, type_value

        if variation_point == ar.VariationPointEnum.NumericalValueSpecification:
            enum_name_value = spec.ShortLabel.split('_')
            if len(enum_name_value) == 2:  # maybe ENUM
                enum_ref = abs_ref(dn, enum_name_value[0])
                if self.references.enums.get_impl_ref(enum_ref):
                    type_class = r2.TypeClass.Enum
                    type_value = r2.EnumValue(*enum_name_value)
                    return type_class, type_value

        # APPLICATION to IMPLEMENTATION mapping
        if base_type:
            app_ref = base_type
        else:
            short_name = spec.ShortLabel
            app_ref = abs_ref(dn, short_name)

        dt_type, impl_ref = self.dt_ctx.impl_ref_from_app_ref(app_ref)

        if dt_type == 'APPLICATION-PRIMITIVE-DATA-TYPE':
            r2_type = self.references.primitives.get_impl_ref(impl_ref)
            base_type = r2_type.BaseType
            value = spec.VariationPoint.Value
            type_class, type_value = r2.TypeClass.Primitive, r2.PrimitiveValue(base_type, value)

        elif dt_type == 'APPLICATION-ARRAY-DATA-TYPE':
            r2_type = self.references.arrays.get_impl_ref(impl_ref)

            elements = []
            for el_spec in spec.VariationPoint.Elements:
                el_type_class, el_type_value = self.resolve_variation_point(dn, el_spec, r2_type.TypeRef)
                elements.append((el_type_class, el_type_value))

            ar_base_type = r2_type.Name
            type_class = r2.TypeClass.Array
            type_value = r2.ArrayValue(ar_base_type, elements)

        elif dt_type == 'APPLICATION-RECORD-DATA-TYPE':
            r2_type = self.references.records.get_impl_ref(impl_ref)

            fields = []
            for el_r2_type, el_spec in zip(r2_type.RecordElements, spec.VariationPoint.Fields):
                el_type_class, el_type_value = self.resolve_variation_point(dn, el_spec, el_r2_type.TypeRef)
                el_name = el_spec.ShortLabel
                fields.append((el_type_class, el_name, el_type_value))

            ar_base_type = r2_type.Name
            type_class = r2.TypeClass.Record
            type_value = r2.RecordValue(ar_base_type, fields)

        elif dt_type == 'COMPU-METHOD':
            # r2_type = self.references.enums.get_impl_ref(impl_ref)
            raise Exception(f'TODO: {dt_type}')

        else:
            raise Exception(f'NotImplemented {dt_type}')

        return type_class, type_value


class DatatypeScope:
    """
    Data Types, declarations and scope
    """
    References = namedtuple('References', [
        'enums',
        'modes',
        'primitives',
        'arrays',
        'records'
    ])

    def enums(self, pkg_name):
        return self.references.enums(pkg_name)

    def modes(self, pkg_name):
        return self.references.modes(pkg_name)

    def primitives(self, pkg_name):
        return self.references.primitives(pkg_name)

    def arrays(self, pkg_name):
        return self.references.arrays(pkg_name)

    def records(self, pkg_name):
        return self.references.records(pkg_name)


    def __init__(self, ar_packages):
        self.ar_packages = ar_packages
        self.dt_ctx = DataTypesContext(ar_packages)

        self.references = None
        self.constants = None

    def _build_references(self):

        enums = EnumSpecification(self.ar_packages, self.dt_ctx)
        modes = ModeSpecification(self.ar_packages, self.dt_ctx)
        primitive = PrimitiveSpecification(self.ar_packages, self.dt_ctx)
        array = ArraySpecification(self.ar_packages, self.dt_ctx)
        records = RecordSpecification(self.ar_packages, self.dt_ctx)

        references = DatatypeScope.References(enums, modes, primitive, array, records)
        return references

    def _build_constants(self):
        constants = ConstantSpecification(self.ar_packages, self.dt_ctx, self.references)
        return constants

    def build(self):

        self.references = self._build_references()
        self.constants = self._build_constants()

        return self

    def dereference(self, ref):
        dt = self.references.primitives.get_app_ref(ref)
        if dt:
            return r2.TypeClass.Primitive, dt
        dt = self.references.arrays.get_app_ref(ref)
        if dt:
            return r2.TypeClass.Array, dt
        dt = self.references.records.get_app_ref(ref)
        if dt:
            return r2.TypeClass.Record, dt
        dt = self.references.enums.get_app_ref(ref)
        if dt:
            return r2.TypeClass.Enum, dt


if __name__ == '__main__':
    import fart.ar.model as arm
    # model_dir = '../test/autosar/ComponentTest'
    # model_dir = '../test/autosar/DataTypeMappingArrayTest/'
    # model_dir = '../test/autosar/DataTypeMappingSetTest/'
    model_dir = '../../test/autosar/AllGrammarElements'
    #model_dir = '../../test/autosar/ValueSpecificationTypeTest'

    ar_pkg = arm.workspace(model_dir)

    scope = DatatypeScope(ar_pkg)
    scope.build()

    print(scope.constants.constant_specification)
