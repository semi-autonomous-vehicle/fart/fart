# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from pathlib import Path

import fart.ar.transformers as art
from fart import artypes as ar
import fart.ar.parser as parser


def print_cb(path, pkg):
    print(path, pkg.ShortName)


def visit_packages(path, pkg, preorder_cb=None, postorder_cb=None):
    """
    """
    for ch in pkg.ARPackage:
        new_path = tuple(list(path) + [ch.ShortName])

        if preorder_cb:
            preorder_cb(new_path, ch)

        visit_packages(new_path, ch, preorder_cb, postorder_cb)

        if postorder_cb:
            postorder_cb(new_path, ch)


def workspace(model_dir):

    autosar_components = []
    for path in Path(model_dir).rglob('*.arxml'):
        etag = parser.parse(path.as_posix())
        autosar = art.AutosarTransformer(etag)
        autosar_components.append(autosar)

    if not len(autosar_components):
        raise(FileExistsError(f'Empty ARXML project in: {model_dir}'))

    pkgs = sum([ar_part.ARPackage for ar_part in autosar_components], [])
    unlevel = ar.Autosar(pkgs)

    ar_packages = {}

    def flatten_cb(path, pkg):

        if path in ar_packages.keys():
            lhs = ar_packages[path]
            ar_packages[path] = ar.ARPackage(lhs.ShortName,
                                             lhs.Elements + pkg.Elements,
                                             lhs.ARPackage + pkg.ARPackage)
        else:
            ar_packages[path] = pkg

    visit_packages((), unlevel, preorder_cb=flatten_cb)

    return ar_packages


if __name__ == '__main__':

    model_dir = '../../test/autosar/AllGrammarElements'

    ar_packages = workspace(model_dir)

    for k, v in ar_packages.items():
        print(k, len(v.Elements))
