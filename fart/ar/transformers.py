# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from fart import artypes as ar
from fart.utils.termcolor import bcolors


def selectFeature(etag, *tags, default=None):
    text = default
    path = './' + '/'.join(tags)
    target = etag.find(path)

    if target is not None:
        text = target.text

    return text


def selectFeatureAttr(etag, attr_name, *tags, default=None):
    text = default
    attr = None
    path = './' + '/'.join(tags)
    target = etag.find(path)

    if target is not None:
        text = target.text
        attr = target.attrib[attr_name]

    return attr, text


def selectNamed(etag, name, *tags):
    path = './' + '/'.join(tags) + '[text()="{name}"]'

    return etag.xpath(path)


def selectElements(etag, *tags):
    path = '.' + '/'.join(tags)

    return etag.findall(path)


def maybeBoolean(txt):
    if txt.lower() == 'false':
        return False

    if txt.lower() == 'true':
        return False

    return None


# DataSendCompletedEventTransformer
def DataSendCompletedEventTransformer(epkg, etag):
    source = ar.DataSendCompletedEvent()
    source.StartOnEvent = selectFeature(etag, 'RTE-EVENT', 'START-ON-EVENT-REF')
    source.EventSource = selectFeature(etag, 'DATA-SEND-COMPLETED-EVENT', 'EVENT-SOURCE')

    return source


def ApplicationArrayDataTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    category = selectFeature(etag, 'CATEGORY')
    element = None

    el = selectElements(etag, 'ELEMENT')
    if len(el):
        el_short_name = selectFeature(el[0], 'SHORT-NAME')
        el_type = selectFeature(el[0], 'TYPE-TREF')
        el_max_number_of_elements = selectFeature(el[0], 'MAX-NUMBER-OF-ELEMENTS')
        element = ar.ApplicationArrayElement(el_short_name, el_type, el_max_number_of_elements)

    return ar.ApplicationArrayDataType(short_name, category, element)


def ApplicationPrimitiveDataTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    category = selectFeature(etag, 'CATEGORY')
    base_type = selectFeature(etag, 'SW-DATA-DEF-PROPS', 'SW-DATA-DEF-PROPS-VARIANTS',
                              'SW-DATA-DEF-PROPS-CONDITIONAL', 'BASE-TYPE-REF')
    compu_method = selectFeature(etag, 'SW-DATA-DEF-PROPS', 'SW-DATA-DEF-PROPS-VARIANTS',
                              'SW-DATA-DEF-PROPS-CONDITIONAL', 'COMPU-METHOD-REF')
    return ar.ApplicationPrimitiveDataType(short_name, category, base_type, compu_method)


def ImplementationDataTypeElementTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    category = selectFeature(etag, 'CATEGORY')
    array_size = selectFeature(etag, 'ARRAY-SIZE')
    array_size_semantics = selectFeature(etag, 'ARRAY-SIZE-SEMANTICS')
    implementation_data_type_ref = selectFeature(etag,
                                                 'SW-DATA-DEF-PROPS',
                                                 'SW-DATA-DEF-PROPS-VARIANTS',
                                                 'SW-DATA-DEF-PROPS-CONDITIONAL',
                                                 'IMPLEMENTATION-DATA-TYPE-REF')

    sub_elements = []
    for el in selectElements(etag, 'SUB-ELEMENTS', 'IMPLEMENTATION-DATA-TYPE-ELEMENT'):
        element_typ = ImplementationDataTypeElementTransformer(epkg, el)
        sub_elements.append(element_typ)

    return ar.ImplementationDataTypeElement(short_name, category, array_size, array_size_semantics,
                                            implementation_data_type_ref, sub_elements)


def ImplementationDataTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    category = selectFeature(etag, 'CATEGORY')
    base_type = selectFeature(etag, 'SW-DATA-DEF-PROPS', 'SW-DATA-DEF-PROPS-VARIANTS',
                              'SW-DATA-DEF-PROPS-CONDITIONAL', 'BASE-TYPE-REF')
    sub_elements = []
    for el in selectElements(etag, 'SUB-ELEMENTS', 'IMPLEMENTATION-DATA-TYPE-ELEMENT'):
        element_typ = ImplementationDataTypeElementTransformer(epkg, el)
        sub_elements.append(element_typ)

    return ar.ImplementationDataType(short_name, category, base_type, sub_elements)


def DataTypeMappingSetTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')

    mapping_set = []
    for el in selectElements(etag, 'DATA-TYPE-MAPS', 'DATA-TYPE-MAP'):
        dest, application_data_type_ref = selectFeatureAttr(el,
                                                            'DEST',
                                                            'APPLICATION-DATA-TYPE-REF')
        implementation_data_type_ref = selectFeature(el, 'IMPLEMENTATION-DATA-TYPE-REF')
        mapping_set.append(ar.DataTypeMap((dest, application_data_type_ref),
                                          implementation_data_type_ref))

    return ar.DataTypeMappingSet(short_name, mapping_set, [])


def ApplicationRecordDataTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    desc = selectFeature(etag, 'DESC', 'L-2')
    category = selectFeature(etag, 'CATEGORY')

    elements = []
    for el_etag in selectElements(etag, 'ELEMENTS', 'APPLICATION-RECORD-ELEMENT'):
        el_short_name = selectFeature(el_etag, 'SHORT-NAME')
        el_type = selectFeature(el_etag, 'TYPE-TREF')
        el_category = selectFeature(el_etag, 'CATEGORY')
        elements.append(ar.ApplicationRecordElement(el_short_name, el_type, el_category))

    return ar.ApplicationRecordDataType(short_name, desc, category, elements)


def ApplicationSwComponentTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    ports = []
    el_ports = selectElements(etag, 'PORTS')
    if len(el_ports):
        ports = PortsTransformer(el_ports[0])

    return ar.ApplicationSwComponentType(short_name, ports)


def ClientServerInterfaceTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    is_service = selectFeature(etag, 'IS-SERVICE')
    possible_errors = [(selectFeature(err, 'SHORT-NAME'), selectFeature(err, 'ERROR-CODE'))
                        for err in selectElements(etag, 'POSSIBLE-ERRORS',
                                                        'APPLICATION-ERROR')]

    operations = []
    for el in selectElements(etag, 'OPERATIONS', 'CLIENT-SERVER-OPERATION'):
        el_short_name = selectFeature(el, 'SHORT-NAME')
        el_possible_errors = [err.text for err in
                              selectElements(el, 'POSSIBLE-ERROR-REFS', 'POSSIBLE-ERROR-REF')]

        arguments = []
        for el2 in selectElements(el, 'ARGUMENTS', 'ARGUMENT-DATA-PROTOTYPE'):
            el2_short_name = selectFeature(el2, 'SHORT-NAME')
            el2_type = selectFeature(el2, 'TYPE-TREF')
            direction = selectFeature(el2, 'DIRECTION')
            arguments.append(ar.ArgumentDataPrototype(el2_short_name, el2_type, direction))

        operations.append(ar.ClientServerOperation(el_short_name, el_possible_errors, arguments))

    return ar.ClientServerInterface(short_name, is_service, operations, possible_errors)


def CompositionSwComponentTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')

    components = []
    for el in selectElements(etag, 'COMPONENTS', 'SW-COMPONENT-PROTOTYPE'):
        el_short_name = selectFeature(etag, 'SHORT-NAME')
        type_tref = selectFeature(etag, 'TYPE-TREF')
        components.append(ar.SwComponentPrototype(el_short_name, type_tref))

    connectors = []
    for el in selectElements(etag, 'CONNECTORS', 'ASSEMBLY-SW-CONNECTOR'):
        el_short_name = selectFeature(etag, 'SHORT-NAME')
        provider = [ar.Provider(selectFeature(pel, 'CONTEXT-COMPONENT-REF'),
                                selectFeature(pel, 'TARGET-P-PORT-REF'))
                    for pel in selectElements(etag, 'PROVIDER-IREF')]
        requester = [ar.Requester(selectFeature(pel, 'CONTEXT-COMPONENT-REF'),
                                  selectFeature(pel, 'TARGET-R-PORT-REF'))
                     for pel in selectElements(etag, 'REQUESTER-IREF')]
        outer_port = None
        target_r_port = None
        target_p_port = None
        context_component = None
        base = None

        connectors.append(ar.AssemblySwConnector(el_short_name, provider, requester,
                                                 outer_port, target_r_port,
                                                 target_p_port, context_component, base))

    ports = []
    el_ports = selectElements(etag, 'PORTS')
    if len(el_ports):
        ports = PortsTransformer(el_ports[0])

    return ar.CompositionSwComponentType(short_name, components, connectors, ports)


def CompuMethodTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    category = selectFeature(etag, 'CATEGORY')

    compu_scales = []
    for el in selectElements(etag, 'COMPU-INTERNAL-TO-PHYS', 'COMPU-SCALES', 'COMPU-SCALE'):
        v_t = selectFeature(el, 'COMPU-CONST', 'VT')
        lower_limit = selectFeature(el, 'LOWER-LIMIT')
        compu_scales.append(ar.CompuScale(v_t, lower_limit))

    return ar.CompuMethod(short_name, category, compu_scales)


# VALUE SPECIFICATION
def ApplicationRuleBasedValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')

    return short_label, ar.ApplicationRuleBasedValueSpecification('NotImplemented')


def ApplicationValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')
    category = selectFeature(etag, 'CATEGORY')

    sw_value_conts = [el.text for el in selectElements(etag, 'SW-AXIS-CONTS', 'SW-AXIS-CONT')]

    return short_label, ar.ApplicationValueSpecification(category, sw_value_conts)


def ArrayValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')

    elements = []
    for el in selectElements(etag, 'ELEMENTS', '*'):
        el_short_label, variation_point_type, variation_point = VariationPointTransformer(el)
        elements.append(ar.ValueSpecification(el_short_label, variation_point_type, variation_point))

    return short_label, ar.ArrayValueSpecification(elements)


def ConstantReferenceTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')
    dest, constant_ref = selectFeatureAttr(etag, 'DEST', 'CONSTANT-REF')

    return short_label, ar.ConstantReference(dest, constant_ref)


def NumericalRuleBasedValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')

    return short_label, ar.NumericalRuleBasedValueSpecification('NotImplemented')


def NumericalValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')
    value = selectFeature(etag, 'VALUE')
    return short_label, ar.NumericalValueSpecification(value)


def RecordValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')

    fields = []
    for el in selectElements(etag, 'FIELDS', '*'):
        el_short_label, variation_point_type, variation_point = VariationPointTransformer(el)
        fields.append(ar.ValueSpecification(el_short_label, variation_point_type, variation_point))

    return short_label, ar.RecordValueSpecification(fields)


def ReferenceValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')
    dest, reference_value_ref = selectFeatureAttr(etag, 'DEST', 'REFERENCE-VALUE-REF')

    return short_label, ar.ReferenceValueSpecification(dest, reference_value_ref)


def TextValueSpecificationTransformer(etag):
    short_label = selectFeature(etag, 'SHORT-LABEL')
    value = selectFeature(etag, 'VALUE')

    return short_label, ar.TextValueSpecification(value)


def VariationPointTransformer(element):
    short_label = None
    variation_point_type = None
    variation_point = None

    tag = element.tag

    if tag == 'APPLICATION-RULE-BASED-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.ApplicationRuleBasedValueSpecification
        short_label, variation_point = ApplicationRuleBasedValueSpecificationTransformer(element)
    elif tag == 'APPLICATION-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.ApplicationValueSpecification
        short_label, variation_point = ApplicationValueSpecificationTransformer(element)
    elif tag == 'ARRAY-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.ArrayValueSpecification
        short_label, variation_point = ArrayValueSpecificationTransformer(element)
    elif tag == 'CONSTANT-REFERENCE':
        variation_point_type = ar.VariationPointEnum.ConstantReference
        short_label, variation_point = ConstantReferenceTransformer(element)
    elif tag == 'NUMERICAL-RULE-BASED-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.NumericalRuleBasedValueSpecification
        short_label, variation_point = NumericalRuleBasedValueSpecificationTransformer(element)
    elif tag == 'NUMERICAL-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.NumericalValueSpecification
        short_label, variation_point = NumericalValueSpecificationTransformer(element)
    elif tag == 'RECORD-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.RecordValueSpecification
        short_label, variation_point = RecordValueSpecificationTransformer(element)
    elif tag == 'REFERENCE-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.ReferenceValueSpecification
        short_label, variation_point = ReferenceValueSpecificationTransformer(element)
    elif tag == 'TEXT-VALUE-SPECIFICATION':
        variation_point_type = ar.VariationPointEnum.TextValueSpecification
        short_label, variation_point = TextValueSpecificationTransformer(element)

    return short_label, variation_point_type, variation_point


def ValueSpecTransformer(etag):
    short_label = None
    variation_point_type = None
    variation_point = None

    chields_el = etag.getchildren()
    if len(chields_el):
        element = chields_el[0]
        short_label, variation_point_type, variation_point = VariationPointTransformer(element)

    return ar.ValueSpecification(short_label, variation_point_type, variation_point)


def ConstantSpecificationTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    el_value_spec = selectElements(etag, 'VALUE-SPEC')

    assert(len(el_value_spec))
    value_spec = ValueSpecTransformer(el_value_spec[0])

    return ar.ConstantSpecification(short_name, value_spec)


def DataConstrTransformer(epkg, etag):
    return ar.DataConstr(*{
        'DataConstrRules': None,
        'InternalConstrs': None,
        'PhysConstrs': None,
        'LowerLimit': None,
        'UpperLimit': None,
    })


def ModeDeclarationGroupTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    mode_declarations = []
    for el in selectElements(etag, 'MODE-DECLARATIONS', 'MODE-DECLARATION'):
        el_short_name = selectFeature(el, 'SHORT-NAME')
        mode_declarations.append(el_short_name)

    initial_mode = None
    if len(mode_declarations):
        initial_mode = selectFeature(etag, 'INITIAL-MODE-REF', default=mode_declarations[0])

    return ar.ModeDeclarationGroup(short_name, mode_declarations, initial_mode)


def ModeDeclarationMappingSetTransformer(epkg, etag):
    return ar.ModeDeclarationMappingSet()


def ModeSwitchInterfaceTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    is_service = selectFeature(etag, 'IS-SERVICE')

    modes = []
    for el in selectElements(etag, 'MODE-GROUP'):
        el_short_name = selectFeature(el, 'SHORT-NAME')
        el_type_tref = selectFeature(el, 'TYPE-TREF')
        modes.append(ar.ModeGroup(el_short_name, el_type_tref))

    return ar.ModeSwitchInterface(short_name, is_service, modes)


def ParameterInterfaceTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    is_service = selectFeature(etag, 'IS-SERVICE')

    parameters = []
    for el in selectElements(etag, 'PARAMETERS', 'PARAMETER-DATA-PROTOTYPE'):
        el_short_name = selectFeature(el, 'SHORT-NAME')
        el_category = selectFeature(el, 'CATEGORY')
        el_type = selectFeature(el, 'TYPE-TREF')
        init_value_el = selectElements(el, 'INIT-VALUE')
        el_init_value = None
        if len(init_value_el):
            el_init_value = ValueSpecTransformer(init_value_el[0])

        parameters.append(ar.ParameterDataPrototype(el_short_name,
                                                    el_category,
                                                    el_type,
                                                    el_init_value))

    return ar.ParameterInterface(short_name, is_service, parameters)


def ParameterSwComponentTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    ports = []
    el_ports = selectElements(etag, 'PORTS')
    if len(el_ports):
        ports = PortsTransformer(el_ports[0])
    data_type_mappings = None

    return ar.ParameterSwComponentType(short_name, ports, data_type_mappings)


def SenderReceiverInterfaceTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    is_service = selectFeature(etag, 'IS-SERVICE')

    data_elements = []

    for el in selectElements(etag, 'DATA-ELEMENTS', 'VARIABLE-DATA-PROTOTYPE'):
        el_short_name = selectFeature(el, 'SHORT-NAME')
        type_tref = selectFeature(el, 'TYPE-TREF')
        sw_impl_policy = selectFeature(el,
                                       'SW-DATA-DEF-PROPS',
                                       'SW-DATA-DEF-PROPS-VARIANTS',
                                       'SW-DATA-DEF-PROPS-CONDITIONAL',
                                       'SW-IMPL-POLICY')

        data_elements.append(ar.VariableDataPrototype(el_short_name, type_tref, sw_impl_policy))

    return ar.SenderReceiverInterface(short_name, is_service, data_elements)


def SensorActuatorSwComponentTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    ports = []
    el_ports = selectElements(etag, 'PORTS')
    if len(el_ports):
        ports = PortsTransformer(el_ports[0])

    return ar.SensorActuatorSwComponentType(short_name, ports)


# SERVER: parse ports
def SenderComSpecGroupTransformer(etag):
    composite_network_representations = selectFeature(etag, 'COMPOSITE-NETWORK-REPRESENTATIONS')
    data_element_ref = selectFeature(etag, 'DATA-ELEMENT-REF')
    handle_out_of_range = selectFeature(etag, 'HANDLE-OUT-OF-RANGE')
    network_representation = selectFeature(etag, 'NETWORK-REPRESENTATION')
    transmission_acknowledge = selectFeature(etag, 'TRANSMISSION-ACKNOWLEDGE')
    uses_end_to_end_protection = selectFeature(etag, 'USES-END-TO-END-PROTECTION')
    # FIXME some
    return ar.SenderComSpecGroup(composite_network_representations,
                                 data_element_ref,
                                 handle_out_of_range,
                                 network_representation,
                                 transmission_acknowledge,
                                 uses_end_to_end_protection)


def ModeSwitchSenderComSpecTransformer(etag):
    enhanced_mode_api = maybeBoolean(selectFeature(etag, 'ENHANCED-MODE-API'))
    mode_group_ref = selectFeature(etag, 'MODE-GROUP-REF')
    mode_switched_ack = selectElements(etag, 'MODE-SWITCHED-ACK') == [] and False or True
    queue_length = selectFeature(etag, 'ENHANCED-MODE-API', default=0)

    return ar.ModeSwitchSenderComSpec(enhanced_mode_api,
                                      mode_group_ref,
                                      mode_switched_ack,
                                      queue_length)


def NonqueuedSenderComSpecTransformer(etag):
    init_value_el = selectElements(etag, 'INIT-VALUE')
    init_value = None
    if len(init_value_el):
        init_value = ValueSpecTransformer(init_value_el[0])

    sender_com_spec_group = SenderComSpecGroupTransformer(etag)

    return ar.NonqueuedSenderComSpec(init_value, sender_com_spec_group)


def NvProvideComSpecTransformer(etag):
    ram_block_init_value_el = selectElements(etag, 'RAM-BLOCK-INIT-VALUE')
    ram_block_init_value = None
    if len(ram_block_init_value_el):
        ram_block_init_value = ValueSpecTransformer(ram_block_init_value_el[0])

    return ar.NvProvideComSpec(ram_block_init_value)


def ParameterProvideComSpecTransformer(etag):
    init_value_el = selectElements(etag, 'INIT-VALUE')
    init_value = None
    if len(init_value_el):
        init_value = ValueSpecTransformer(init_value_el[0])
    parameter_ref = selectFeature(etag, 'PARAMETER-REF')
    return ar.ParameterProvideComSpec(init_value, parameter_ref)


def QueuedSenderComSpecTransformer(etag):
    return ar.QueuedSenderComSpec(SenderComSpecGroupTransformer(etag))


def ServerComSpecTransformer(etag):
    operation_ref = selectFeature(etag, 'OPERATION-REF')
    queue_length = selectFeature(etag, 'QUEUE-LENGTH')
    transformation_com_spec_propss = None  # FIXME

    return ar.ServerComSpec(operation_ref, queue_length, transformation_com_spec_propss)


def ProvidedComSpecsTransformer(etag):
    choice, com_spec = None, None
    for el in etag.getchildren():
        if el.tag == 'MODE-SWITCH-SENDER-COM-SPEC':
            com_spec = ModeSwitchSenderComSpecTransformer(el)
            choice = ar.SenderComSpecEnum.ModeSwitchSenderComSpec
        if el.tag == 'NONQUEUED-SENDER-COM-SPEC':
            com_spec = NonqueuedSenderComSpecTransformer(el)
            choice = ar.SenderComSpecEnum.NonqueuedSenderComSpec
        if el.tag == 'NV-PROVIDE-COM-SPEC':
            com_spec = NvProvideComSpecTransformer(el)
            choice = ar.SenderComSpecEnum.NvProvideComSpec
        if el.tag == 'PARAMETER-PROVIDE-COM-SPEC':
            com_spec = ParameterProvideComSpecTransformer(el)
            choice = ar.SenderComSpecEnum.ParameterProvideComSpec
        if el.tag == 'QUEUED-SENDER-COM-SPEC':
            com_spec = QueuedSenderComSpecTransformer(el)
            choice = ar.SenderComSpecEnum.QueuedSenderComSpec
        if el.tag == 'SERVER-COM-SPEC':
            com_spec = ServerComSpecTransformer(el)
            choice = ar.SenderComSpecEnum.ServerComSpec
    return ar.ProvidedComSpecs(choice, com_spec)


# CLIENT: parse ports
def ClientComSpecTransformer(etag):
    operation_ref = selectFeature(etag, 'OPERATION-REF')
    transformation_com_spec_propss = selectFeature(etag, 'TRANSFORMATION-COM-SPEC-PROPSS')

    return ar.ClientComSpec(operation_ref, transformation_com_spec_propss)


def ModeSwitchReceiverComSpecTransformer(etag):
    enhanced_mode_api = selectFeature(etag, 'ENHANCED-MODE-API')
    mode_group_ref = selectFeature(etag, 'MODE-GROUP-REF')
    supports_asynchronous_mode_switch = selectFeature(etag, 'SUPPORTS-ASYNCHRONOUS-MODE-SWITCH')

    return ar.ModeSwitchReceiverComSpec(enhanced_mode_api,
                                        mode_group_ref,
                                        supports_asynchronous_mode_switch)


def NonqueuedReceiverComSpecTransformer(etag):
    alive_timeout = selectFeature(etag, 'ALIVE-TIMEOUT')
    enable_update = selectFeature(etag, 'ENABLE-UPDATE')
    filter_ = selectFeature(etag, 'FILTER')
    handle_data_status = selectFeature(etag, 'HANDLE-DATA-STATUS')
    handle_never_received = selectFeature(etag, 'HANDLE-NEVER-RECEIVED')
    handle_timeout_type = selectFeature(etag, 'HANDLE-TIMEOUT-TYPE')

    init_value_el = selectElements(etag, 'INIT-VALUE')
    init_value = None
    if len(init_value_el):
        init_value = ValueSpecTransformer(init_value_el[0])

    return ar.NonqueuedReceiverComSpec(alive_timeout,
                                       enable_update,
                                       filter_,
                                       handle_data_status,
                                       handle_never_received,
                                       handle_timeout_type,
                                       init_value)


def NvRequireComSpecTransformer(etag):
    init_value_el = selectElements(etag, 'INIT-VALUE')
    init_value = None
    if len(init_value_el):
        init_value = ValueSpecTransformer(init_value_el[0])
    variable_ref = selectFeature(etag, 'VARIABLE-REF')

    return ar.NvRequireComSpec(init_value, variable_ref)


def ParameterRequireComSpecTransformer(etag):
    init_value_el = selectElements(etag, 'INIT-VALUE')
    init_value = None
    if len(init_value_el):
        init_value = ValueSpecTransformer(init_value_el[0])
    parameter_ref = selectFeature(etag, 'PARAMETER-REF')

    return ar.ParameterRequireComSpec(init_value, parameter_ref)


def ReceiverComSpecGroupTransformer(etag):
    composite_network_representations = selectFeature(etag, 'COMPOSITE-NETWORK-REPRESENTATIONS')
    data_element_ref = selectFeature(etag, 'DATA-ELEMENT-REF')
    external_replacement_ref = selectFeature(etag, 'EXTERNAL-REPLACEMENT-REF')
    handle_out_of_range = selectFeature(etag, 'HANDLE-OUT-OF-RANGE')
    handle_out_of_range_status = selectFeature(etag, 'HANDLE-OUT-OF-RANGE-STATUS')
    max_delta_counter_init = selectFeature(etag, 'MAX-DELTA-COUNTER-INIT')
    max_no_new_or_repeated_data = selectFeature(etag, 'MAX-NO-NEW-OR-REPEATED-DATA')
    network_representation = selectFeature(etag, 'NETWORK-REPRESENTATION')
    replace_with = selectFeature(etag, 'REPLACE-WITH')
    sync_counter_init = selectFeature(etag, 'SYNC-COUNTER-INIT')
    transformation_com_spec_propss = selectFeature(etag, 'TRANSFORMATION-COM-SPEC-PROPSS')
    uses_end_to_end_protection = selectFeature(etag, 'USES-END-TO-END-PROTECTION')

    return ar.ReceiverComSpecGroup(composite_network_representations,
                                   data_element_ref,
                                   external_replacement_ref,
                                   handle_out_of_range,
                                   handle_out_of_range_status,
                                   max_delta_counter_init,
                                   max_no_new_or_repeated_data,
                                   network_representation,
                                   replace_with,
                                   sync_counter_init,
                                   transformation_com_spec_propss,
                                   uses_end_to_end_protection)


def QueuedReceiverComSpecTransformer(etag):
    queue_length = selectFeature(etag, 'QUEUE-LENGTH')
    receiver_com_spec_group = ReceiverComSpecGroupTransformer(etag)

    return ar.QueuedReceiverComSpec(queue_length, receiver_com_spec_group)


def RequiredComSpecsTransformer(etag):
    choice, com_spec = None, None
    for el in etag.getchildren():
        if el.tag == 'CLIENT-COM-SPEC':
            com_spec = ClientComSpecTransformer(el)
            choice = ar.ReceiverComSpecEnum.ClientComSpec
        if el.tag == 'MODE-SWITCH-RECEIVER-COM-SPEC':
            com_spec = ModeSwitchReceiverComSpecTransformer(el)
            choice = ar.ReceiverComSpecEnum.ModeSwitchReceiverComSpec
        if el.tag == 'NONQUEUED-RECEIVER-COM-SPEC':
            com_spec = NonqueuedReceiverComSpecTransformer(el)
            choice = ar.ReceiverComSpecEnum.NonqueuedReceiverComSpec
        if el.tag == 'NV-REQUIRE-COM-SPEC':
            com_spec = NvRequireComSpecTransformer(el)
            choice = ar.ReceiverComSpecEnum.NvRequireComSpec
        if el.tag == 'PARAMETER-REQUIRE-COM-SPEC':
            com_spec = ParameterRequireComSpecTransformer(el)
            choice = ar.ReceiverComSpecEnum.ParameterRequireComSpec
        if el.tag == 'QUEUED-RECEIVER-COM-SPEC':
            com_spec = QueuedReceiverComSpecTransformer(el)
            choice = ar.ReceiverComSpecEnum.QueuedReceiverComSpec

    return ar.RequiredComSpecs(choice, com_spec)


def PPortPrototypeTransformer(etag):
    short_name = selectFeature(etag, 'SHORT-NAME')

    el_provided_com_specs = selectElements(etag, 'PROVIDED-COM-SPECS')
    if len(el_provided_com_specs):
        provided_com_specs = ProvidedComSpecsTransformer(el_provided_com_specs[0])
    else:
        provided_com_specs = None

    provided_interface_tref = selectFeature(etag, 'PROVIDED-INTERFACE-TREF')

    return ar.PPortPrototype(short_name, provided_com_specs, provided_interface_tref)


def RPortPrototypeTransformer(etag):
    short_name = selectFeature(etag, 'SHORT-NAME')

    el_required_com_specs = selectElements(etag, 'REQUIRED-COM-SPECS')
    if len(el_required_com_specs):
        required_com_specs = RequiredComSpecsTransformer(el_required_com_specs[0])
    else:
        required_com_specs = None

    required_interface_tref = selectFeature(etag, 'REQUIRED-INTERFACE-TREF')

    return ar.RPortPrototype(short_name, required_com_specs, required_interface_tref)


def PrPortPrototypeTransformer(etag):
    short_name = selectFeature(etag, 'SHORT-NAME')

    el_provided_com_specs = selectElements(etag, 'PROVIDED-COM-SPECS')
    if len(el_provided_com_specs):
        provided_com_specs = ProvidedComSpecsTransformer(el_provided_com_specs[0])
    else:
        provided_com_specs = None

    el_required_com_specs = selectElements(etag, 'REQUIRED-COM-SPECS')
    if len(el_required_com_specs):
        required_com_specs = RequiredComSpecsTransformer(el_required_com_specs[0])
    else:
        required_com_specs = None

    provided_required_interface_tref = selectFeature(etag, 'PROVIDED-REQUIRED-INTERFACE-TREF')

    return ar.PrPortPrototype(short_name, provided_com_specs,
                              required_com_specs, provided_required_interface_tref)


def PortsTransformer(etag):

    ports = []
    for el in etag.getchildren():
        if el.tag == 'P-PORT-PROTOTYPE':
            spec = PPortPrototypeTransformer(el)
        if el.tag == 'PR-PORT-PROTOTYPE':
            spec = RPortPrototypeTransformer(el)
        if el.tag == 'R-PORT-PROTOTYPE':
            spec = RPortPrototypeTransformer(el)
        ports.append((el.tag, spec))

    return ports


def ServiceSwComponentTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    ports = []
    el_ports = selectElements(etag, 'PORTS')
    if len(el_ports):
        ports = PortsTransformer(el_ports[0])

    return ar.ServiceSwComponentType(short_name, ports)


def SwBaseTypeTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    native_declaration = selectFeature(etag, 'NATIVE-DECLARATION')
    return ar.SwBaseType(short_name, native_declaration)


def SwcBswMappingTransformer(epkg, etag):
    return ar.SwcBswMapping()


def SwcImplementationTransformer(epkg, etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    behavior_ref = selectFeature(etag, 'BEHAVIOR-REF')
    programming_language = selectFeature(etag, 'PROGRAMMING-LANGUAGE')

    compilers = []
    for el in selectElements(etag, 'COMPILERS', 'COMPILER'):
        el_short_name = selectFeature(etag, 'SHORT-NAME')
        vendor = selectFeature(el, 'VENDOR')
        version = selectFeature(el, 'VERSION')
        compilers.append(ar.Compiler(el_short_name, vendor, version))

    return ar.SwcImplementation(short_name, behavior_ref, programming_language, compilers)


def SwcTimingTransformer(epkg, etag):
    return ar.SwcTiming()


elements_map = {
    'APPLICATION-ARRAY-DATA-TYPE': ApplicationArrayDataTypeTransformer,
    'APPLICATION-PRIMITIVE-DATA-TYPE': ApplicationPrimitiveDataTypeTransformer,
    'APPLICATION-RECORD-DATA-TYPE': ApplicationRecordDataTypeTransformer,
    'APPLICATION-SW-COMPONENT-TYPE': ApplicationSwComponentTypeTransformer,
    'CLIENT-SERVER-INTERFACE': ClientServerInterfaceTransformer,
    'COMPOSITION-SW-COMPONENT-TYPE': CompositionSwComponentTypeTransformer,
    'COMPU-METHOD': CompuMethodTransformer,
    'CONSTANT-SPECIFICATION': ConstantSpecificationTransformer,
    'DATA-CONSTR': DataConstrTransformer,
    'DATA-TYPE-MAPPING-SET': DataTypeMappingSetTransformer,
    'IMPLEMENTATION-DATA-TYPE': ImplementationDataTypeTransformer,
    'MODE-DECLARATION-GROUP': ModeDeclarationGroupTransformer,
    'MODE-DECLARATION-MAPPING-SET': ModeDeclarationMappingSetTransformer,
    'MODE-SWITCH-INTERFACE': ModeSwitchInterfaceTransformer,
    'PARAMETER-INTERFACE': ParameterInterfaceTransformer,
    'PARAMETER-SW-COMPONENT-TYPE': ParameterSwComponentTypeTransformer,
    'SENDER-RECEIVER-INTERFACE': SenderReceiverInterfaceTransformer,
    'SENSOR-ACTUATOR-SW-COMPONENT-TYPE': SensorActuatorSwComponentTypeTransformer,
    'SERVICE-SW-COMPONENT-TYPE': ServiceSwComponentTypeTransformer,
    'SW-BASE-TYPE': SwBaseTypeTransformer,
    'SWC-BSW-MAPPING': SwcBswMappingTransformer,
    'SWC-IMPLEMENTATION': SwcImplementationTransformer,
    'SWC-TIMING': SwcTimingTransformer,
}


# ARPackageTransformer
def ARPackageTransformer(etag):
    short_name = selectFeature(etag, 'SHORT-NAME')
    ar_elements = etag.find('ELEMENTS')

    if ar_elements is not None:
        source_elements = ar_elements.getchildren()
    else:
        source_elements = []

    elements = []
    for el in source_elements:
        tag = el.tag
        hnd = elements_map.get(tag, None)
        if not hnd:
            print(bcolors.WARNING + f'Warning: element {tag} not supported.' + bcolors.ENDC)
        else:
            elements.append((tag, hnd(etag, el)))

    ar_packages = etag.findall('AR-PACKAGES/AR-PACKAGE')
    packages = []
    for etag in ar_packages:
        pkg = ARPackageTransformer(etag)
        packages.append(pkg)

    return ar.ARPackage(short_name, elements, packages)


def AutosarTransformer(etag):
    ar_packages = etag.findall('AR-PACKAGES/AR-PACKAGE')
    packages = []
    for etag in ar_packages:
        pkg = ARPackageTransformer(etag)
        packages.append(pkg)
    source = ar.Autosar(packages)
    return source


if __name__ == '__main__':
    from pathlib import Path

    import fart.ar.parser as arxml
    import fart.ar.transformers as art

    model_dir = '../../test/autosar/AllGrammarElements'
    autosar_components = []
    for path in Path(model_dir).rglob('*.arxml'):
        print(path)
        etag = arxml.parse(path.as_posix())
        autosar = art.AutosarTransformer(etag)
        autosar_components.append(autosar)

    print(autosar_components)
