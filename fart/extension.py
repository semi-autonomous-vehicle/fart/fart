import inspect
import sys
import os
import types
import argparse
from importlib import import_module

import fart.cli


class CommandExtension:
    """
    The extension point for 'command' extensions.

    The following properties must be defined:
    * `NAME` (will be set to the entry point name)

    The following methods must be defined:
    * `main`

    The following methods can be defined:
    * `add_arguments`
    """

    NAME = None

    def __init__(self):
        super(CommandExtension, self).__init__()

    def add_arguments(self, parser, cli_name, *, argv=None):
        pass

    def main(self, *, parser, args):
        raise NotImplementedError()


class VerbExtension:
    """
    The extension point for 'fart' verb extensions.

    The following properties must be defined:
    * `NAME` (will be set to the entry point name)

    The following methods must be defined:
    * `main`

    The following methods can be defined:
    * `add_arguments`
    """

    NAME = None

    def __init__(self):
        super(VerbExtension, self).__init__()

    def add_arguments(self, parser, cli_name):
        pass

    def main(self, *, args):
        raise NotImplementedError()


class MutableString:

    def __init__(self):
        self.value = ''

    def __getattr__(self, name):
        return getattr(self.value, name)

    def __iter__(self):
        return self.value.__iter__()


class SuppressUsageOutput:

    def __init__(self, parsers):
        self._parsers = parsers
        self._callbacks = {}

    def __enter__(self):  # noqa: D105
        for p in self._parsers:
            self._callbacks[p] = p.print_help, p.exit
            p.print_help = lambda: None
            p.exit = types.MethodType(_ignore_zero_exit(p.exit), p)
        return self

    def __exit__(self, *args):  # noqa: D105
        for p, callbacks in self._callbacks.items():
            p.print_help, p.exit = callbacks


def get_entry_points(group_name):
    entry_points = {}

    for cmd in fart.cli.commands:
        lib = import_module('.'.join(['fart', 'cli', cmd]))

        for entry_point in lib.entry_points[group_name]:
            name, cls = entry_point
            module = import_module('.'.join(['fart', 'cli', cmd, group_name, name]))
            try:
                ctor = getattr(module, cls)
            except AttributeError:
                raise Exception(f"entry {cls} not found in {name}!")
            entry_points[name] = ctor()

    return entry_points


def get_command_extensions(group_name, *, exclude_names=None):
    extensions = instantiate_extensions(
        group_name, exclude_names=exclude_names)
    for name, extension in extensions.items():
        extension.NAME = name
    return extensions


def instantiate_extensions(group_name, *, exclude_names=None, unique_instance=False):
    extension_instances = {}
    return extension_instances


def _ignore_zero_exit(original_exit_handler):
    def exit_(self, status=0, message=None):
        nonlocal original_exit_handler
        if status == 0:
            return
        return original_exit_handler(status=status, message=message)
    return exit_


def _is_completion_requested():
    return os.environ.get('_ARGCOMPLETE') == '1'


def get_first_line_doc(any_type):
    if not any_type.__doc__:
        return ''
    lines = any_type.__doc__.splitlines()
    if not lines:
        return ''
    if lines[0]:
        line = lines[0]
    elif len(lines) > 1:
        line = lines[1]
    return line.strip().rstrip('.')


def add_subparsers_on_demand(
    parser, cli_name, dest, group_name,
    required=True, argv=None
):
    """
    Create argparse subparser for each extension on demand.
    """
    mutable_description = MutableString()
    subparser = parser.add_subparsers(
        title='Commands', description=mutable_description,
        metavar=f'Call `{cli_name} <command> -h` for more detailed usage.')
    subparser.dest = ' ' + dest.lstrip('_')
    subparser.required = required
    entry_points = get_entry_points(group_name)

    command_parsers = {}
    for name in sorted(entry_points.keys()):
        command_parser = subparser.add_parser(
            name,
            formatter_class=argparse.RawDescriptionHelpFormatter)
        command_parsers[name] = command_parser

    root_parser = getattr(parser, '_root_parser', parser)
    with SuppressUsageOutput({parser} | set(command_parsers.values())):
        args = argv
        # for completion use the arguments provided by the argcomplete env var
        if _is_completion_requested():
            from argcomplete import split_line
            _, _, _, comp_words, _ = split_line(os.environ['COMP_LINE'])
            args = comp_words[1:]
        try:
            known_args, _ = root_parser.parse_known_args(args=args)
        except SystemExit:
            if not _is_completion_requested():
                raise
            # if the partial arguments can't be parsed use no known args
            known_args = argparse.Namespace(**{subparser.dest: None})

    name = getattr(known_args, subparser.dest)
    # add description for the selected command extension to the subparser
    command_extensions = entry_points
    if name is None:
        if command_extensions:
            description = ''
            max_length = max(len(name) for name in command_extensions.keys())
            for name in sorted(command_extensions.keys()):
                extension = command_extensions[name]
                description += '%s  %s\n' % (
                    name.ljust(max_length), get_first_line_doc(extension))
                command_parser = command_parsers[name]
                command_parser.set_defaults(**{dest: extension})
            mutable_description.value = description
    else:
        extension = command_extensions[name]
        command_parser = command_parsers[name]
        command_parser.set_defaults(**{dest: extension})
        command_parser.description = get_first_line_doc(extension)

        # add the arguments for the requested extension
        if hasattr(extension, 'add_arguments'):
            command_parser._root_parser = root_parser
            signature = inspect.signature(extension.add_arguments)
            kwargs = {}
            if 'argv' in signature.parameters:
                kwargs['argv'] = argv
            extension.add_arguments(
                command_parser, f'{cli_name} {name}', **kwargs)
            del command_parser._root_parser

    return subparser
