# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from collections import namedtuple
from enum import auto, Flag


Autosar = namedtuple('Autosar', [
    'ARPackage',
])

# ARPackageTransformer.java
ARPackage = namedtuple('ARPackage', [
    'ShortName',
    'Elements',
    'ARPackage',
])

ApplicationPrimitiveDataType = namedtuple('ApplicationPrimitiveDataType', [
    'ShortName',
    'Category',
    'BaseType',
    'CompuMethod'
])

ApplicationSwComponentType = namedtuple('ApplicationSwComponentType', [
    'ShortName',
    'Ports'
])

ImplementationDataTypeElement = namedtuple('ImplementationDataTypeElement', [
    'ShortName',
    'Category',
    'ArraySize',
    'ArraySizeSemantics',
    'ImplementationDataTypeRef',
    'SubElements',
])

ImplementationDataType = namedtuple('ImplementationDataType', [
    'ShortName',
    'Category',
    'BaseType',
    'SubElements'
])

DataTypeMap = namedtuple('DataTypeMap', [
    'ApplicationDataTypeRef',
    'ImplementationDataTypeRef'
])

# DataTypeMappingSetTransformer.java
DataTypeMappingSet = namedtuple('DataTypeMappingSet', [
    'ShortName',
    'DataTypeMaps',
    'ModeRequestTypeMaps',
])


SwBaseType = namedtuple('SwBaseType', [
    'ShortName',
    'NativeDeclaration'
])

# AbstractPRPortPrototypeTransformer.java
AbstractPRPortPrototype = namedtuple('AbstractPRPortPrototype', [

])


# ApplicationArrayElementTransformer.java
ApplicationArrayElement = namedtuple('ApplicationArrayElement', [
    'ShortName',
    'Type',
    'MaxNumberOfElements'
])

ApplicationArrayDataType = namedtuple('ApplicationArrayDataType', [
    'ShortName',
    'Category',
    'Element'
])


# ApplicationErrorTransformer.java
ApplicationError = namedtuple('ApplicationError', [
    'ErrorCode',
])

# ApplicationRecordElementTransformer.java
ApplicationRecordElement = namedtuple('ApplicationRecordElement', [
    'ShortName',
    'Type',
    'Category',
])

ApplicationRecordDataType = namedtuple('ApplicationRecordDataType', [
    'ShortName',
    'Desc',
    'Category',
    'Elements'
])


# ArgumentDataPrototypeTransformer.java
ArgumentDataPrototype = namedtuple('ArgumentDataPrototype', [
    'Type',
    'Direction',
    'ServerArgumentImplPolicy',
])

# ArrayTypeTransformer.java
ArrayType = namedtuple('ArrayType', [
    'Element',
    'ArraySizeSemantics',
    'MaxNumberOfElements',
    'Category',
    'BaseType',
    'SwDataDefProps',
])


# ArTypedPerInstanceMemoryTransformer.java
ArTypedPerInstanceMemory = namedtuple('ArTypedPerInstanceMemory', [
    'Category',
    'Type',
    'InitValue',
    'SwDataDefProps',
    # addSupportedFeature(SwDataDefPropertiesTransformer.SUPPORTED_FEATURES);
    # addSupportedFeature(getValueSpecificationFactory().getSupportedFeatures());
])

# AssemblySwConnectorTransformer.java
Provider = namedtuple('Provider', [
    'ContextComponentRef',
    'TargetPPortRef'
])

Requester = namedtuple('Requester', [
    'ContextComponentRef',
    'TargetRPortRef'
])

AssemblySwConnector = namedtuple('AssemblySwConnector', [
    'ShortName',
    'Provider',
    'Requester',
    'OuterPort',
    'TargetRPort',
    # 'ContextComponent',
    # 'Base',
    'TargetPPort',
    'ContextComponent',
    'Base',
])

# AsynchronousServerCallReturnsEventTransformer.java
AsynchronousServerCallReturnsEvent = namedtuple('AsynchronousServerCallReturnsEvent', [
    'StartOnEvent',
    'EventSource',
    'AsynchronousServerCallPoint',
])

# AtomicSwComponentTypeTransformer.java
AtomicSwComponentType = namedtuple('AtomicSwComponentType', [
    'Ports',
    'InternalBehaviors',
])

# BitfieldTransformer.java
Bitfield = namedtuple('Bitfield', [
    'Category',
    'CompuInternalToPhys',
    'CompuContent',
    'CompuScaleContents',
    'CompuScales',
    'LowerLimit',
    'ShortLabel',
    'Mixed',
    'Mask',
    # 'CompuScaleContents',
    'CompuConst',
    'CompuConstContentType',
    'Vt',
])

# BooleanTypeTransformer.java
BooleanType = namedtuple('BooleanType', [
    'SwDataDefProps',
    'SwDataDefPropsVariants',
    'InvalidValue',
    'BaseType',
    # 'SwDataDefPropsVariants',
    'Category',
])

# ClientServerInterfaceTransformer.java
ClientServerInterface = namedtuple('ClientServerInterface', [
    'ShortName',
    'IsService',
    'Operations',
    'PossibleErrors',
])

# ClientServerOperationTransformer.java
ArgumentDataPrototype = namedtuple('ArgumentDataPrototype', [
    'ShortName',
    'TypeTref',
    'Direction'
])

ClientServerOperation = namedtuple('ClientServerOperation', [
    'ShortName',
    'PossibleErrors',
    'Arguments',
])

# CodeDescriptorTransformer.java
CodeDescriptor = namedtuple('CodeDescriptor', [
    'ArtifactDescriptors',
    'Category',
    'ShortLabel',
])

# CompilerTransformer.java
Compiler = namedtuple('Compiler', [
    'ShortName',
    'Vendor',
    'Version',
])

# ComplexDeviceDriverSwComponentTypeTransformer.java
ComplexDeviceDriverSwComponentType = namedtuple('ComplexDeviceDriverSwComponentType', [
    'Ports',
])

# CompositionSwComponentTypeTransformer.java

CompositionSwComponentType = namedtuple('CompositionSwComponentType', [
    'ShortName',
    'Components',
    'Connectors',
    'Ports',
])

# ConstantSepecificationMappingSetTransformer.java
ConstantSepecificationMappingSet = namedtuple('ConstantSepecificationMappingSet', [
    'Mappings',
])

# ConstantSpecificationTransformer.java

ApplicationRuleBasedValueSpecification = namedtuple('ApplicationRuleBasedValueSpecification', [
    'Category'
])

ApplicationValueSpecification = namedtuple('ApplicationValueSpecification', [
    'Category',
    'SwValueConts'
])

ArrayValueSpecification = namedtuple('ArrayValueSpecification', [
    'Elements'                  # ValueSpecification
])

ConstantReference = namedtuple('ConstantReference', [
    'Dest',
    'ConstantRef'
])

NumericalRuleBasedValueSpecification = namedtuple('NumericalRuleBasedValueSpecification', [
    'RuleBasedValues'
])

NumericalValueSpecification = namedtuple('NumericalValueSpecification', [
    'Value'                     # MyEnum_TRUE
])

RecordValueSpecification = namedtuple('RecordValueSpecification', [
    'Fields'
])

ReferenceValueSpecification = namedtuple('ReferenceValueSpecification', [
    'Dest',
    'ReferenceValueRef'
])

TextValueSpecification = namedtuple('TextValueSpecification', [
    'Value'
])


class VariationPointEnum(Flag):
    ApplicationRuleBasedValueSpecification = auto()
    ApplicationValueSpecification = auto()
    ArrayValueSpecification = auto()
    ConstantReference = auto()
    NumericalRuleBasedValueSpecification = auto()
    NumericalValueSpecification = auto()
    RecordValueSpecification = auto()
    ReferenceValueSpecification = auto()
    TextValueSpecification = auto()


ValueSpecification = namedtuple('ValueSpecification', [
    'ShortLabel',
    'VariationPointType',
    'VariationPoint'
])

ConstantSpecification = namedtuple('ConstantSpecification', [
    'ShortName',
    'ValueSpecification',
])

# DataConstrTransformer.java
DataConstr = namedtuple('DataConstr', [
    'DataConstrRules',
    'InternalConstrs',
    'PhysConstrs',
    'LowerLimit',
    'UpperLimit',
    # 'LowerLimit',
    # 'UpperLimit',
])

# DataElementPrototypeTransformer.java
DataElementPrototype = namedtuple('DataElementPrototype', [
    'Type',
    'SwDataDefProps',
    'SwDataDefPropsVariants',
    'SwImplPolicy',
    'InitValue',
    # 'SwDataDefProps',
    # addSupportedFeature(SwDataDefPropertiesTransformer.SUPPORTED_FEATURES);
    # addSupportedFeature(getValueSpecificationFactory().getSupportedFeatures());
])

# DataReadAccessTransformer.java
DataReadAccess = namedtuple('DataReadAccess', [
    'AccessedVariable',
    'AutosarVariable',
    'TargetDataPrototype',
    'PortPrototype',
    'Base',
])

# DataReceivedEventTransformer.java
DataReceivedEvent = namedtuple('DataReceivedEvent', [
    'TargetDataElement',
    'ContextRPort',
    'Base',
    'StartOnEvent',
    'Data',
])

# DataReceiveErrorEventTransformer.java
DataReceiveErrorEvent = namedtuple('DataReceiveErrorEvent', [
    'StartOnEvent',
    'Data',
    'TargetDataElement',
    'ContextRPort',
    'Base',
])

# DataReceivePointTransformer.java
DataReceivePoint = namedtuple('DataReceivePoint', [
    'AccessedVariable',
    'AutosarVariable',
    'TargetDataPrototype',
    'PortPrototype',
    'Base',
])

# DataSendCompletedEventTransformer.java
DataSendCompletedEvent = namedtuple('DataSendCompletedEvent', [
    'StartOnEvent',
    'EventSource',
])

# DataSendPointTransformer.java
DataSendPoint = namedtuple('DataSendPoint', [
    'AccessedVariable',
    'AutosarVariable',
    'TargetDataPrototype',
    'PortPrototype',
    'Base',
])

# DataWriteAccessTransformer.java
DataWriteAccess = namedtuple('DataWriteAccess', [
    'AccessedVariable',
    'AutosarVariable',
    'TargetDataPrototype',
    'PortPrototype',
    'Base',
])

# DataWriteCompletedEventTransformer.java
DataWriteCompletedEvent = namedtuple('DataWriteCompletedEvent', [
    'StartOnEvent',
    'EventSource',
])

# DelegationSwConnectorTransformer.java
DelegationSwConnector = namedtuple('DelegationSwConnector', [
    'InnerPort',
    'OuterPort',
    'ContextComponent',
    'TargetRPort',
    # 'ContextComponent',
    # 'TargetPPort',
    'Base',
])

# EcuAbstractionSwComponentTypeTransformer.java
EcuAbstractionSwComponentType = namedtuple('EcuAbstractionSwComponentType', [
    'Ports',
])

CompuScale = namedtuple('CompuScale', [
    'VT',
    'LowerLimit'
])

CompuMethod = namedtuple('CompuMethod', [
    'ShortName',
    'Category',
    'CompuScales'
])

# EnumTransformer.java
Enum = namedtuple('Enum', [
    'CompuInternalToPhys',
    'CompuContent',
    'CompuScaleContents',
    'UpperLimit',
    # 'CompuScaleContents',
    'CompuConst',
    'CompuConstContentType',
    'Vt',
    'SwDataDefProps',
    'CompuMethod',
    'SwDataDefPropsVariants',
    'Category',
    'Mixed',
])

# FixedPointTypeTransformer.java
FixedPointType = namedtuple('FixedPointType', [
    'CompuInternalToPhys',
    'CompuContent',
    'CompuScaleContents',
    'CompuDenominator',
    'CompuNumerator',
    'CompuRationalCoeffs',
    'CompuMethod',
    'SwDataDefPropsVariants',
    'Mixed',
])

# ImplementationTransformer.java
Implementation = namedtuple('Implementation', [
    'UsedCodeGenerator',
    'ProgrammingLanguage',
    'RequiredRTEVendor',
    'Behavior',
    'CodeDescriptors',
    'Compilers',
    # 'Behavior',
    'SwVersion',
    'VendorId',
    'ResourceConsumption',
])

# IntegerTypeAbstractTransformer.java
IntegerTypeAbstract = namedtuple('IntegerTypeAbstract', [
    'BaseType',
    'DataConstr',
    'InvalidValue',
    'Unit',
    'DataConstrRules',
    'PhysConstrs',
    'LowerLimit',
    'UpperLimit',
    'SwDataDefPropsVariants',
    'Category',
    'SwDataDefProps',
    'Mixed',
    'IntervalType',
])

# IntegerTypeTransformer.java
IntegerType = namedtuple('IntegerType', [
    'CompuMethod',
])

# InternalBehaviorTransformer.java
InternalBehavior = namedtuple('InternalBehavior', [
    'PortAPIOptions',
    'SupportsMultipleInstantiation',
    'AtomicSwComponentType',
    'IndirectAPI',
    'Events',
    'ExclusiveAreas',
    'ExplicitInterRunnableVariables',
    'ImplicitInterRunnableVariables',
    'PerInstanceParameters',
    'PerInstanceMemories',
    'ArTypedPerInstanceMemories',
    'ServiceDependencies',
    'Runnables',
    'SharedParameters',
    # 'ServiceDependencies',
    'DataTypeMappings',
    'IncludedDataTypeSets',
    'DataTypes',
    'LiteralPrefix',
])

# InterRunnableVariableTransformer.java
InterRunnableVariable = namedtuple('InterRunnableVariable', [
    'Type',
    'InitValue',
    'ShortLabel',
    'Constant',
])

# Messages.java

# ModeDeclarationGroupPrototypeTransformer.java
ModeDeclarationGroupPrototype = namedtuple('ModeDeclarationGroupPrototype', [
    'Type',
])

# ModeDeclarationGroupTransformer.java
ModeDeclarationGroup = namedtuple('ModeDeclarationGroup', [
    'ShortName',
    'ModeDeclarations',
    'InitialMode',
])

# ModeReferenceTransformer.java
ModeReference = namedtuple('ModeReference', [
])

# ModeSwitchInterfaceTransformer.java
ModeGroup = namedtuple('ModeGroup', [
    'ShortName',
    'TypeTref'
])

ModeSwitchInterface = namedtuple('ModeSwitchInterface', [
    'ShortName',
    'IsService',
    'ModeGroup',
])

# ModeSwitchPointTransformer.java
ModeSwitchPoint = namedtuple('ModeSwitchPoint', [
    'ModeGroup',
    'TargetModeGroup',
    'ContextPPort',
    'Base',
])

# ModeSwitchSenderComSpecCreator.java

# OperationInvokedEventTransformer.java
OperationInvokedEvent = namedtuple('OperationInvokedEvent', [
    'StartOnEvent',
    'Operation',
    'TargetProvidedOperation',
    'ContextPPort',
    'Base',
])

# ParamAccessTransformer.java
ParamAccess = namedtuple('ParamAccess', [
    'AccessedParameter',
    'LocalParameter',
    'AutosarParameter',
    'PortPrototype',
    'TargetDataPrototype',
    'RootParameterDataPrototype',
])

# ParameterDataPrototypeTransformer.java
ParameterDataPrototype = namedtuple('ParameterDataPrototype', [
    'ShortName',
    'Category',
    'TypeTref',
    'InitValue',
    # 'SwDataDefProps',
    # addSupportedFeature(SwDataDefPropertiesTransformer.SUPPORTED_FEATURES);
    # addSupportedFeature(getValueSpecificationFactory().getSupportedFeatures());
])

# ParameterInterfaceTransformer.java
ParameterInterface = namedtuple('ParameterInterface', [
    'ShortName',
    'IsService',
    'Parameters',
])

# ParameterSwComponentTypeTransformer.java
ParameterSwComponentType = namedtuple('ParameterSwComponentType', [
    'ShortName',
    'Ports',
    'DataTypeMappings',
])

# PerInstanceMemoryTransformer.java
PerInstanceMemory = namedtuple('PerInstanceMemory', [
    'Type',
    'TypeDefinition',
])

# PhysicalDimensionTransformer.java
PhysicalDimension = namedtuple('PhysicalDimension', [
    'CurrentExp',
    'LengthExp',
    'LuminousIntensityExp',
    'MassExp',
    'MolarAmountExp',
    'TemperatureExp',
    'TimeExp',
])

# PPortPrototypeTransformer.java
# SERVER
# MODE-SWITCH-SENDER-COM-SPEC
# NONQUEUED-SENDER-COM-SPEC
# NV-PROVIDE-COM-SPEC
# PARAMETER-PROVIDE-COM-SPEC
# QUEUED-SENDER-COM-SPEC
# SERVER-COM-SPEC

# ModeSwitchSenderComSpec
# NonqueuedSenderComSpec
# NvProvideComSpec
# ParameterProvideComSpec
# QueuedSenderComSpec
# ServerComSpec


class SenderComSpecEnum(Flag):
    ModeSwitchSenderComSpec = auto()
    NonqueuedSenderComSpec = auto()
    NvProvideComSpec = auto()
    ParameterProvideComSpec = auto()
    QueuedSenderComSpec = auto()
    ServerComSpec = auto()


# '''
#     COMPOSITE-NETWORK-REPRESENTATIONS
#     DATA-ELEMENT-REF
#     HANDLE-OUT-OF-RANGE
#     NETWORK-REPRESENTATION
#     TRANSMISSION-ACKNOWLEDGE
#     USES-END-TO-END-PROTECTION
# '''


SenderComSpecGroup = namedtuple('SenderComSpecGroup', [
    'CompositeNetworkRepresentations',
    'DataElementRef',
    'HandleOutOfRange',
    'NetworkRepresentation',
    'TransmissionAcknowledge',
    'UsesEndToEndProtection',
])

# '''
#     ENHANCED-MODE-API
#     MODE-GROUP-REF
#     MODE-SWITCHED-ACK
#     QUEUE-LENGTH
# '''

ModeSwitchSenderComSpec = namedtuple('ModeSwitchSenderComSpec', [
    'EnhancedModeApi',
    'ModeGroupRef',
    'ModeSwitchedAck',
    'QueueLength',
])

# '''
#     INIT-VALUE,
#     SENDER-COM-SPEC-GROUP,
# '''
NonqueuedSenderComSpec = namedtuple('NonqueuedSenderComSpec', [
    'InitValue',
    'SenderComSpecGroup',
])
#    RAM-BLOCK-INIT-VALUE
NvProvideComSpec = namedtuple('NvProvideComSpec', [
    'RamBlockInitValue'
])

# '''
#     INIT-VALUE
#     PARAMETER-REF
# '''

ParameterProvideComSpec = namedtuple('ParameterProvideComSpec', [
    'InitValue',
    'ParameterRef',
])

#    SENDER-COM-SPEC-GROUP
QueuedSenderComSpec = namedtuple('QueuedSenderComSpec', [
    'SenderComSpecGroup'
])

# '''
#     OPERATION-REF
#     QUEUE-LENGTH
#     TRANSFORMATION-COM-SPEC-PROPSS
# '''

ServerComSpec = namedtuple('ServerComSpec', [
    'OperationRef',
    'QueueLength',
    'TransformationComSpecPropss',
])

ProvidedComSpecs = namedtuple('ProvidedComSpecs', [
    'choice',
    'com_spec'
])

# CLIENT
# CLIENT-COM-SPEC
# MODE-SWITCH-RECEIVER-COM-SPEC
# NONQUEUED-RECEIVER-COM-SPEC
# NV-REQUIRE-COM-SPEC
# PARAMETER-REQUIRE-COM-SPEC
# QUEUED-RECEIVER-COM-SPEC

# ClientComSpec
# ModeSwitchReceiverComSpec
# NonqueuedReceiverComSpec
# NvRequireComSpec
# ParameterRequireComSpec
# QueuedReceiverComSpec


class ReceiverComSpecEnum(Flag):
    ClientComSpec = auto()
    ModeSwitchReceiverComSpec = auto()
    NonqueuedReceiverComSpec = auto()
    NvRequireComSpec = auto()
    ParameterRequireComSpec = auto()
    QueuedReceiverComSpec = auto()


# '''
#     OPERATION-REF
#     TRANSFORMATION-COM-SPEC-PROPSS
# '''

ClientComSpec = namedtuple('ClientComSpec', [
    'OperationRef',
    'TransformationComSpecPropss',
])

# '''
#     ENHANCED-MODE-API
#     MODE-GROUP-REF
#     SUPPORTS-ASYNCHRONOUS-MODE-SWITCH
# '''

ModeSwitchReceiverComSpec = namedtuple('ModeSwitchReceiverComSpec', [
    'EnhancedModeApi',
    'ModeGroupRef',
    'SupportsAsynchronousModeSwitch',
])

# '''    ALIVE-TIMEOUT
#     ENABLE-UPDATE
#     FILTER
#     HANDLE-DATA-STATUS
#     HANDLE-NEVER-RECEIVED
#     HANDLE-TIMEOUT-TYPE
#     INIT-VALUE
# '''

NonqueuedReceiverComSpec = namedtuple('NonqueuedReceiverComSpec', [
    'AliveTimeout',
    'EnableUpdate',
    'Filter',
    'HandleDataStatus',
    'HandleNeverReceived',
    'HandleTimeoutType',
    'InitValue',
])

# '''
#     INIT-VALUE
#     VARIABLE-REF
# '''

NvRequireComSpec = namedtuple('NvRequireComSpec', [
    'InitValue',
    'VariableRef',
])

# '''
#     INIT-VALUE
#     PARAMETER-REF
# '''

ParameterRequireComSpec = namedtuple('ParameterRequireComSpec', [
    'InitValue',
    'ParameterRef',
])

# '''
#     COMPOSITE-NETWORK-REPRESENTATIONS
#     DATA-ELEMENT-REF
#     EXTERNAL-REPLACEMENT-REF
#     HANDLE-OUT-OF-RANGE
#     HANDLE-OUT-OF-RANGE-STATUS
#     MAX-DELTA-COUNTER-INIT
#     MAX-NO-NEW-OR-REPEATED-DATA
#     NETWORK-REPRESENTATION
#     REPLACE-WITH
#     SYNC-COUNTER-INIT
#     TRANSFORMATION-COM-SPEC-PROPSS
#     USES-END-TO-END-PROTECTION
# '''

ReceiverComSpecGroup = namedtuple('ReceiverComSpecGroup', [
    'CompositeNetworkRepresentations',
    'DataElementRef',
    'ExternalReplacementRef',
    'HandleOutOfRange',
    'HandleOutOfRangeStatus',
    'MaxDeltaCounterInit',
    'MaxNoNewOrRepeatedData',
    'NetworkRepresentation',
    'ReplaceWith',
    'SyncCounterInit',
    'TransformationComSpecPropss',
    'UsesEndToEndProtection',
])

# '''
#     QUEUE-LENGTH
#     RECEIVER-COM-SPEC-GROUP
# '''

QueuedReceiverComSpec = namedtuple('QueuedReceiverComSpec', [
    'QueueLength',
    'ReceiverComSpecGroup',
])

RequiredComSpecs = namedtuple('RequiredComSpecs', [
    'choice',
    'com_spec'
])

PPortPrototype = namedtuple('PPortPrototype', [
    'ShortName',
    'ProvidedComSpecs',
    'ProvidedInterfaceTref',
])

# RPortPrototypeTransformer.java
RPortPrototype = namedtuple('RPortPrototype', [
    'ShortName',
    'RequiredComSpecs',
    'RequiredInterfaceTref',
])

# PRPortPrototypeTransformer.java
PRPortPrototype = namedtuple('PRPortPrototype', [
    'ShortName',
    'ProvidedComSpecs',
    'RequiredComSpecs',
    'ProvidedRequiredInterfaceTref',

])

# RealTypeTransformer.java
RealType = namedtuple('RealType', [
    'LowerLimit',
    'UpperLimit',
    'InvalidValue',
    'BaseType',
    'DataConstr',
    'CompuMethod',
    'SwDataDefProps',
    'SwDataDefPropsVariants',
    'Category',
    'Mixed',
    'IntervalType',
])

# RecordTypeTransformer.java
RecordType = namedtuple('RecordType', [
    'Elements',
    'Category',
])


# RunnableEntityTransformer.java
RunnableEntity = namedtuple('RunnableEntity', [
    'CanBeInvokedConcurrently',
    'CanEnterExclusiveAreas',
    'MinimumStartInterval',
    'ReadLocalVariables',
    'Symbol',
    'RunsInsideExclusiveAreas',
    'WrittenLocalVariables',
    'ParameterAccess',
    'DataReadAccess',
    'DataReceivePointByValues',
    'ModeAccessPoints',
    'DataSendPoints',
    'DataWriteAccess',
    'SwcInternalBehavior',
    'ModeSwitchPoints',
    # 'ParameterAccess',
    'ServerCallPoints',
    'WaitPoints',
    'DataReceivePointByArguments',
    'AccessedVariable',
    'LocalVariable',
    'AsynchronousServerCallResultPoints',
])

# SenderReceiverInterfaceTransformer.java
VariableDataPrototype = namedtuple('VariableDataPrototype', [
    'ShortName',
    'TypeTref',
    'SwImplPolicy'
])

SenderReceiverInterface = namedtuple('SenderReceiverInterface', [
    'ShortName',
    'IsService',
    'DataElements',
])

# SensorActuatorSwComponentTypeTransformer.java
SensorActuatorSwComponentType = namedtuple('SensorActuatorSwComponentType', [
    'ShortName',
    'Ports',
])

# ServerCallPointTransformer.java
ServerCallPoint = namedtuple('ServerCallPoint', [
    'Operation',
    'Timeout',
    'ContextRPort',
    'TargetRequiredOperation',
    'Base',
])

SwcImplementation = namedtuple('SwcImplementation', [
    'ShortName',
    'BehaviorRef',
    'ProgrammingLanguage',
    'Compilers'
])


ServiceSwComponentType = namedtuple('ServiceSwComponentType', [
    'ShortName',
    'Ports',
])

# StringTypeTransformer.java
StringType = namedtuple('StringType', [
    'InvalidValue',
    'BaseType',
    'SwDataDefProps',
    'Category',
    'SwDataDefPropsVariants',
])

# SwcdAr4xArtextTransformerFactory.java
# Artext = namedtuple('Artext', [
# 			return new ResourceConsumptionTransformer(aResourceConsumption);
# ])

# SwcModeSwitchEventTransformer.java
SwcModeSwitchEvent = namedtuple('SwcModeSwitchEvent', [
    'StartOnEvent',
    'Activation',
    'Modes',
    'TargetModeGroup',
    'ContextRPort',
    'ContextModeDeclarationGroupPrototype',
    'TargetModeDeclaration',
    'ContextPort',
])

# SwComponentPrototypeTransformer.java
SwComponentPrototype = namedtuple('SwComponentPrototype', [
    'ShortName',
    'TypeTref',
])

# SwcServiceDependencyTransformer.java
SwcServiceDependency = namedtuple('SwcServiceDependency', [
    'AssignedDatas',
    'Role',
    'UsedDataElement',
    'UsedParameterElement',
    'ServiceNeeds',
    'NDataSets',
    'Reliability',
    'ResistantToChangedSw',
    'RestoreAtStart',
    'StoreAtShutdown',
    'WritingFrequency',
])

# SwDataDefPropertiesTransformer.java

# TimingEventTransformer.java
TimingEvent = namedtuple('TimingEvent', [
    'Period',
    'StartOnEvent',
])

# UnitTransformer.java
Unit = namedtuple('Unit', [
    'FactorSiToUnit',
    'OffsetSiToUnit',
    'PhysicalDimension',
])

# ValueSpecificationFactoryAr40.java

# WaitPointTransformer.java
WaitPoint = namedtuple('WaitPoint', [
    'Timeout',
    'Trigger',
])
