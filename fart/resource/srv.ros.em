// generated from rosidl_adapter/resource/srv.ros.em
// generated code does not contain a copyright notice

@{
TEMPLATE(
    'struct.ros.em',
	pkg_name=pkg_name,
	scope=scope,
    msg=srv.Request,
)
}@
---
@{
TEMPLATE(
    'struct.ros.em',
	pkg_name=pkg_name,
	scope=scope,
   	msg=srv.Response,
)
}@
