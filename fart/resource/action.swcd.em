// generated from rosidl_adapter/resource/action.idl.em
// with input from @(pkg_name)/@(relative_input_file)
// generated code does not contain a copyright notice

package @(pkg_name)

@{
from fart.ros.convert import get_include_file
include_files = set()
fields = action.goal.fields + action.result.fields + action.feedback.fields
for field in fields:
    include_file = get_include_file(field.type)
    if include_file is not None:
        include_files.add(include_file)
}@
import ros.primitive_types.*
@[for include_file in sorted(include_files)]@
import @(include_file)
@[end for]@

@{
TEMPLATE(
    'struct.swcd.em',
    msg=action.goal,
)
}@

@{
TEMPLATE(
    'struct.swcd.em',
    msg=action.result,
)
}@

@{
TEMPLATE(
    'struct.swcd.em',
    msg=action.feedback,
)
}@

interface clientServer service @(action.action_name)_Interface {
    operation  @(action.action_name) {
        in @(action.goal.msg_name)
        inout @(action.feedback.msg_name)
        out @(action.result.msg_name)
    }
}
