@#typedefs for arrays need to be defined outside of the struct
@{
from collections import OrderedDict

from fart.ros.convert import get_swcd_type
from fart.ros.convert import to_swcd_literal
from fart.ros.convert import string_to_swcd_string_literal

typedefs = OrderedDict()
def get_swcd_type_identifier(idl_type):
    return idl_type.replace('::', '__') \
        .replace('<', '__').replace('>', '') \
        .replace('[', '__').replace(']', '')
}@
@[for field in msg.fields]@
@{
idl_type = get_swcd_type(field.type)
}@
@[  if field.type.is_fixed_size_array()]@
@{
idl_base_type = idl_type.split('[', 1)[0]
idl_base_type_identifier = idl_base_type.replace('::', '__')
# only necessary for complex types
if idl_base_type_identifier != idl_base_type:
    if idl_base_type_identifier not in typedefs:
        typedefs[idl_base_type_identifier] = idl_base_type
    else:
        assert typedefs[idl_base_type_identifier] == idl_base_type
idl_type_identifier = get_swcd_type_identifier(idl_type) + '[' + str(field.type.array_size) + ']'
if idl_type_identifier not in typedefs:
    typedefs[idl_type_identifier] = idl_base_type_identifier
else:
    assert typedefs[idl_type_identifier] == idl_base_type_identifier
}@
@[  end if]@
@[end for]@
@[for k, v in typedefs.items()]@
    typedef @(v) @(k);
@[end for]@
@[if msg.constants]@
@[  for constant in msg.constants]@
const @(get_swcd_type(constant.type)) @(constant.name) = @(to_swcd_literal(get_swcd_type(constant.type), constant.value))
@[  end for]@

@[end if]@
@#
@[if msg.annotations.get('comment', [])]@
@@desc { @('\n'.join(msg.annotations['comment'])) }
@[end if]@
record @(msg.msg_name) {
@[if msg.fields]@
@[  for i, field in enumerate(msg.fields)]@
@[if i > 0]@

@[end if]@
@[    if field.annotations.get('comment', [])]@
    @@desc { @('\n'.join(field.annotations['comment'])) }
@[    end if]@
@[    if field.default_value is not None]@
      @@default (value=@(to_swcd_literal(get_swcd_type(field.type), field.default_value)))
@[    end if]@
@[    if 'unit' in field.annotations]@
      @@unit (value=@(string_to_swcd_string_literal(field.annotations['unit'])))
@[    end if]@
@{
idl_type = get_swcd_type(field.type)
}@
@[    if field.type.is_fixed_size_array()]@
@{
idl_type = get_swcd_type_identifier(idl_type)
}@
@[    end if]@
    @(idl_type) @(field.name)@ @(i < len(msg.fields) - 1 ? ',')@
@[  end for]@
@[end if]@

}
