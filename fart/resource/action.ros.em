

@{
TEMPLATE(
    'struct.ros.em',
	pkg_name=pkg_name,
	scope=scope,
    msg=action.Goal,
)
}@
---
@{
TEMPLATE(
    'struct.ros.em',
	pkg_name=pkg_name,
	scope=scope,
    msg=action.Result,
)
}@
---
@{
TEMPLATE(
    'struct.ros.em',
	pkg_name=pkg_name,
	scope=scope,
    msg=action.Feedback,
)
}@
