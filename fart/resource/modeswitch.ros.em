
@[for idx, field in enumerate(mode_switch.ModeValue.Modes)]@
@(field) = @(idx)@

@[end for]@

@(mode_switch.ModeName) = @(mode_switch.ModeValue.Initial)
