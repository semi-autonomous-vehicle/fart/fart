// generated from rosidl_adapter/resource/srv.swcd.em
// with input from @(pkg_name)/@(relative_input_file)
// generated code does not contain a copyright notice

package @(pkg_name)

@{
from fart.ros.convert import get_include_file
include_files = set()
for field in srv.request.fields + srv.response.fields:
    include_file = get_include_file(field.type)
    if include_file is not None:
        include_files.add(include_file)
}@
import basetypes.*
@[for include_file in sorted(include_files)]@
import @(include_file)
@[end for]@

@{
TEMPLATE(
    'struct.swcd.em',
    msg=srv.request,
)
}@

@{
TEMPLATE(
    'struct.swcd.em',
    msg=srv.response,
)
}@

interface clientServer service @(srv.srv_name)_Interface {
    operation  @(srv.srv_name) {
        in @(srv.request.msg_name)
        out @(srv.response.msg_name)
    }
}
