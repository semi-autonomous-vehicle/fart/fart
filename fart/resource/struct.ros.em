@{
from fart.ros.dtypes import get_ros_typedef
from fart.ros.dtypes import to_ros_literal

}@
@[for field in msg.Fields]@
@{
ros_type, ros_def = get_ros_typedef(pkg_name, scope, field.Name, field.TypeRef)
}@
@(ros_type) @(ros_def)@

@[end for]@