Entity Mapping
======================================================================================

.. contents:: Contents
   :local:
   :backlinks: none


System Models
---------------------------------------------------------------------------------------

ROS
~~~

* System

   Complex systems with more nodes running simultaneously

* Node

   Each node in ROS should be responsible for a single, module purpose.
   Each node can send and receive data to other nodes via topics, services, actions, or parameters.

* Service[Client Server]

   Services are method of communication for nodes based on a call-and-response model

* Parameters

   A parameter is a configuration value of a node. You can think of parameters as node settings.

* Subscriber
* Publisher
* Topic

   Topics are one of the main ways in which data is moved between nodes and therefore between different parts of the system.

* Interface (ROS2)
* Actions (ROS2)

   Actions are one of the communication types in ROS 2 and are intended for long running tasks. They consist of three parts: a goal, feedback, and a result.

* Package

   A package can be considered a container for your ROS2 code.

AUTOSAR
~~~~~~~

AUTOSAR meta model releases and a number of related services including
AUTOSAR XSD compliant serialization, rule-based validation, tree and
form-based views and editing, and template-based target code,
documentation and report generation, and more.

The highest (most abstract) description level is the Virtual
Functional Bus (VFB):
* Software components

  component -> behavior -> implementation

* Compositions

  Ports

* Interfaces
  * Client-Server
  * Sender-Receiver

ARText by Artop
~~~~~~~~~~~~~~~

The `AUTOSAR Tool Platform (Artop) <https://www.artop.org/artext/>`_
is an implementation of common base functionality for AUTOSAR
development tools.

ARText is a framework for building textual modeling languages for AUTOSAR.


Example:

.. code-block:: ruby

    package CalculatorPkg

    int UInt8 min 0 max 255
    int UInt16 min 0 max 65535

    interface clientServer Operations {
    	operation Multiply {
    		in UInt8 factor1
    		in UInt8 factor2
    		out UInt16 result
    	}
    }

    interface senderReceiver Operands {
    	data UInt8 op1
    	data UInt8 op2
    }

    interface senderReceiver Result {
    	data UInt16 res
    }

    component application Calculator {
    	ports {
    		server Operations provides Operations
    		receiver AdditionInput requires Operands
    		sender AdditionOutput provides Result
    	}
    }

    internalBehavior CalculatorBehavior for Calculator {
    	runnable Multiply[0.0] {
    		operationInvokedEvent Operations.Multiply
    	}

    	runnable Cysclic [0.0] {
    		timingEvent 0.1
    		dataSendPoint AdditionOutput.res
    		dataReceivePoint AdditionInput.*
    	}
    }


Software Component Language a Syntax Reference :download:`pdf <../_static/swcd.pdf>`

Runtime mappings
---------------------------------------------------------------------------------------

ROS: Package -> executable* -> Node*
AUTOSAR: ECU -> Runnable*
VFB: Composition SWC -> SWC* -> ports*

.. csv-table::
   :header: "AUTOSAR",  "ROS", "Remarks"
   :widths:  20, 30, 50

   ECU, Package, processor deployment
   package, namespace, msg and svc namespaces
   composition, system, VFB mapped on pubsub network
   component application, executable, contain runnables
   runnable, node,
   ports, interfaces,
   param, parameters,
   timingEvent, timer, node periodic execution
   implementation, build-type, code generation for ApplicationSoftwareComponentType
   modeSwitch, none, runnable state management
   none, action, maybe use inout datatype for action notification
   delegate port, none, not used in DDS context
