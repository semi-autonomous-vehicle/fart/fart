Autosar
======================================================================================


AUTOSAR BASICS
---------------------------------------------------------------------------------------


What is Autosar?
~~~~~~~~~~~~~~~~

AUTOSAR (AUTomotive Open System ARchitecture) is an open and
standardized automotive software architecture, jointly developed by
automobile manufacturers, suppliers and tool developers. The
AUTOSAR-standard enables the use of a component based software design
model for the design of a vehicular system. The design model uses
application software components which are linked through an abstract
component, named the virtual function bus.

`AUTOSAR BASICS <https://automotivetechis.wordpress.com/autosar-concepts/>`_

`AUTOSAR Tutorials <https://autosartutorials.com/>`_

`Adptive AUTOSAR release <https://www.autosar.org/nc/document-search/?tx_sysgsearch_pi1%5Bquery%5D=&tx_sysgsearch_pi1%5Bcategory%5D%5B145%5D=145&tx_sysgsearch_pi1%5Bcategory%5D%5B146%5D=146&tx_sysgsearch_pi1%5Bcategory%5D%5B147%5D=147&tx_sysgsearch_pi1%5Bcategory%5D%5B32%5D=32&tx_sysgsearch_pi1%5Bcategory%5D%5B148%5D=148>`_

The VFB is implemented through Autosar Runtime Environment (RTE) and
the layer of Basic Software

Through Autosar Real-time Environment, RTE, the software components
can communicate without being mapped to specific hardware or ECU

The application layer, the software layer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* The functionality of the applications reside in the application layer

* This is the only part of an Autosar system that doesn ́t consist of
  standardized software

* A Software Component, SWC, is the smallest part of a software
  application that has a specific functionality

Communication between software components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A SWC can communicate in two different ways


* Client/server

   The client initiates the communication and requests a service from the server

* Sender/receiver

   The sender expects no answer from the receiver and there will be no answer

   The receiver desides on it's own how and when to act on the information

   The interface structure is responsible for the communi-cation and
   the sender doesn't know who the receiver is or if there are more
   than one receiver
