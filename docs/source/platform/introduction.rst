Introduction
======================================================================================

.. contents:: Contents
   :local:
   :backlinks: none

Fatal Accident Reconstruction Team
---------------------------------------------------------------------------------------

QUOTE: Traffic collision reconstruction is the process of
investigating, analyzing, and drawing conclusions about the causes and
events during a vehicle collision. Reconstructionists conduct
collision analysis and reconstruction to identify the cause of a
collision and contributing factors including the role of the
driver(s), vehicle(s), roadway and general environment. Physics and
engineering principles are the basis for these analyses and may
involved the use of software for calculations and simulations.

Modeling platform
---------------------------------------------------------------------------------------
