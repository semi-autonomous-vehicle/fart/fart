Installation
======================================================================================

.. contents:: Contents
   :local:
   :backlinks: none

ROS2 environment setup
---------------------------------------------------------------------------------------

.. code-block:: bash

  mkdir -p ~/fart_ws/src
  cd ~/fart_ws
  wget https://gitlab.com/semi-autonomous-vehicle/fart/fart/-/raw/master/fart.repos
  vcs import src < fart.repos

  # compile workspace
  colcon build --symlink-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=ON

  # add headers to indexer
  compdb -p build list > compile_commands.json
