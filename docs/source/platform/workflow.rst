Workflow
======================================================================================

.. contents:: Contents
   :local:
   :backlinks: none


Command line interface
---------------------------------------------------------------------------------------

.. code:: bash

    fart --ros-workspace `ros_dev_ws` --artop-workspace `artop_dev_ws` --autosar `autosar`

    fart interface packages           # list of packages that provide interfaces
    fart interface package pkg_name   # list of available interface types within one package
    fart interface create pkg_name    # create empty package in the workspace


    fart rte create pkg_name    # greate ROS nodes for AUTOSAR runnables
    fart test create pkg_name   # create tests for component application

    fart pkg create pkg_name  # Create a new ROS 2 package
    fart pkg list             # list of available packages

    # Interface with ROS interfaces
    fart ros translate `ros_pkg` # Translate ROS package to SWCD interfaces

    fart ar


Model transformation pipeline
---------------------------------------------------------------------------------------


.. uml::
    :scale: 100 %
    :align: center

    title something

    |#dee4e8|R2|
    |#c4c9cd|SWCD|
    |#c4c9cd|AR|
    |#daf0fe|Runtime|

    |SWCD|
    start
        :Import package;
        if (native ROS package) then (yes)
            |R2|
            :fart ros translate pkg;            
        else
            |SWCD|
            :import pkg **ArText**;
        endif
    :Export workspace **ArTop**;    
    :Recompile **ArTop**;    
    |AR|
    :fart pkg create pkg_name;
    :fart interface create pkg_name;    
    |Runtime|
    :fart test create pkg_name;
    :catkin build;

    stop


Export and Recompile SWCD package
---------------------------------------------------------------------------------------

.. code:: bash

    java -jar /opt/artext-demo/artext-compiler-1.5.1-SNAPSHOT.jar \
        -ar Autosar40 -outputFile autosar_compiled/Calculator.arxml  \
        examples/Calculator/Calculator.swcd 


Integrate ROS interfaces
---------------------------------------------------------------------------------------


Discover ROS interfaces

.. code:: bash

    ros2 interface packages

    action_msgs
    action_tutorials_interfaces
    actionlib_msgs
    builtin_interfaces
    composition_interfaces
    diagnostic_msgs
    ...

    fart -a /tmp/workspace ros translate  example_interfaces