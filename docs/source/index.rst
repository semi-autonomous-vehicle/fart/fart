.. FART documentation master file

Welcome to FART's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: PLATFORM

   platform/index

.. toctree::
   :maxdepth: 2
   :caption: AUTOSAR

   autosar/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
