from setuptools import setup

package_name = 'fart'

setup(
    name=package_name,
    version='0.0.2',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
         ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools', 'parameterized', 'pyem'],
    zip_safe=True,
    maintainer='ibnHatab',
    maintainer_email='lib.aca55a@gmail.com',
    description='ROS4AUTOSAR RTE generator',
    license='LGPLv3',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'fart = fart.generator:main',
        ],
    },
)
