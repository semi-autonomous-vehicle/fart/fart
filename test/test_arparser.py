# Copyright 2015 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from glob import glob
import os

import unittest

from fart.ar import model

from parameterized import parameterized


# import pytest
path_to_autosar_test = os.path.dirname(__file__)


class TestArParser(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        pass

    @parameterized.expand(glob(path_to_autosar_test + '/autosar/*'))
    def test_parse_only_ar_models(self, model_dir):
        ar_packages = model.workspace(model_dir)
        assert(len(ar_packages))
