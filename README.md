# FART PLATFORM 

[![pipeline status](https://gitlab.com/semi-autonomous-vehicle/fart/fart/badges/master/pipeline.svg)](https://gitlab.com/semi-autonomous-vehicle/fart/fart/commits/master)

## Overview

`Fatal Accident Reconstruction Team` Modeling Platform. 

Traffic collision reconstruction is the process of
investigating, analyzing, and drawing conclusions about the causes and
events during a vehicle collision. 

Reconstructionists conduct collision analysis and reconstruction to identify the cause of a
collision and contributing factors including the role of the
driver(s), vehicle(s), roadway and general environment. 

Physics and engineering principles are the basis for these analyses and may
involved the use of `software for calculations` and `simulations`.

## Design

See the [project documentation](https://fart.readthedocs.io/en/latest/index.html).
